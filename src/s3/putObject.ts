import { S3 } from 'aws-sdk';
import s3storage from './s3storage';

interface IputObjectArgs {
  body: S3.Body;
  bucket: string;
  fileKey: string;
  ACL?: string;
  ContentType?: string;
}

interface IputObject {
  (args: IputObjectArgs): Promise<S3.PutObjectOutput>
}

const putObject: IputObject = ({ body, bucket, fileKey, ...rest }) => {
  return new Promise((resolve, reject) => {
    s3storage.putObject({
      Body: body,
      Bucket: bucket,
      Key: fileKey,
      ...rest,
    }, (err, data) => {
      if (err) {
        console.error(err, err.stack);
        reject();
      } else {
        resolve(data);
      }
    });
  });
};

export default putObject;
