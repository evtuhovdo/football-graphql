import {MigrationInterface, QueryRunner} from "typeorm";

export class challangeTableId1599364248952 implements MigrationInterface {
    name = 'challangeTableId1599364248952'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_700c7aea6538afffa9c317a6393"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_460e0380f9baa71928980e3fac5"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_b1b0db5176687ba92868223aac7"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "UQ_410af624378ff4f150a3cb463e3"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" RENAME COLUMN "tableId" TO "table_id"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" RENAME COLUMN "tableId" TO "table_id"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" RENAME COLUMN "tableId" TO "table_id"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "UQ_bc108f181bc73f04e8036514a5b" UNIQUE ("sportsman_id", "exercise_id", "league_id", "table_id")`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_b4a993102e2ad717b38d36dea27" FOREIGN KEY ("table_id") REFERENCES "table_2x2"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_b0eb182ebe32abdb55f39a8552d" FOREIGN KEY ("table_id") REFERENCES "table_1x1"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_449373f4711326d5846b24e8317" FOREIGN KEY ("table_id") REFERENCES "table_exercise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_449373f4711326d5846b24e8317"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_b0eb182ebe32abdb55f39a8552d"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_b4a993102e2ad717b38d36dea27"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "UQ_bc108f181bc73f04e8036514a5b"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" RENAME COLUMN "table_id" TO "tableId"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" RENAME COLUMN "table_id" TO "tableId"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" RENAME COLUMN "table_id" TO "tableId"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "UQ_410af624378ff4f150a3cb463e3" UNIQUE ("sportsman_id", "exercise_id", "league_id", "tableId")`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_b1b0db5176687ba92868223aac7" FOREIGN KEY ("tableId") REFERENCES "table_exercise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_460e0380f9baa71928980e3fac5" FOREIGN KEY ("tableId") REFERENCES "table_1x1"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_700c7aea6538afffa9c317a6393" FOREIGN KEY ("tableId") REFERENCES "table_2x2"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
