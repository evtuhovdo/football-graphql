import {MigrationInterface, QueryRunner} from "typeorm";

export class tableExerciseLinkExercises1599060797172 implements MigrationInterface {
    name = 'tableExerciseLinkExercises1599060797172'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "table_exercises_link_exercises" ("tableExerciseId" uuid NOT NULL, "exerciseId" integer NOT NULL, CONSTRAINT "PK_1cb23cc8a056e3de152daaafeb3" PRIMARY KEY ("tableExerciseId", "exerciseId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_7d41137d3f2c04d56d02a3cefe" ON "table_exercises_link_exercises" ("tableExerciseId") `);
        await queryRunner.query(`CREATE INDEX "IDX_96412522b13c2e2ace8d9d77cd" ON "table_exercises_link_exercises" ("exerciseId") `);
        await queryRunner.query(`ALTER TABLE "table_exercises_link_exercises" ADD CONSTRAINT "FK_7d41137d3f2c04d56d02a3cefe2" FOREIGN KEY ("tableExerciseId") REFERENCES "table_exercise"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "table_exercises_link_exercises" ADD CONSTRAINT "FK_96412522b13c2e2ace8d9d77cd0" FOREIGN KEY ("exerciseId") REFERENCES "exercise"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "table_exercises_link_exercises" DROP CONSTRAINT "FK_96412522b13c2e2ace8d9d77cd0"`);
        await queryRunner.query(`ALTER TABLE "table_exercises_link_exercises" DROP CONSTRAINT "FK_7d41137d3f2c04d56d02a3cefe2"`);
        await queryRunner.query(`DROP INDEX "IDX_96412522b13c2e2ace8d9d77cd"`);
        await queryRunner.query(`DROP INDEX "IDX_7d41137d3f2c04d56d02a3cefe"`);
        await queryRunner.query(`DROP TABLE "table_exercises_link_exercises"`);
    }

}
