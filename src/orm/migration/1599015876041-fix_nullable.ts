import {MigrationInterface, QueryRunner} from "typeorm";

export class fixNullable1599015876041 implements MigrationInterface {
    name = 'fixNullable1599015876041'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_f2bf76878b91cd3b2469095512d"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_b1873b5c8ec0ed6551c7dbd6b04"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_85ce9ec2374d9fd0b5016ff5ae4"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_700c7aea6538afffa9c317a6393"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ALTER COLUMN "composition_left_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ALTER COLUMN "composition_right_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ALTER COLUMN "league_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ALTER COLUMN "tableId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_d067a46af3e5c788c1eb2c97665"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_8a68b62be0133ef992cdb64478d"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_5051cad541f47b888057593f3e0"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_460e0380f9baa71928980e3fac5"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ALTER COLUMN "sportsman_left_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ALTER COLUMN "sportsman_right_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ALTER COLUMN "league_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ALTER COLUMN "tableId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" DROP CONSTRAINT "FK_5e38f296fd8caf6d95bb8b1f4b1"`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" DROP CONSTRAINT "FK_8a92be3c84735c426d7c321ea8b"`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" ALTER COLUMN "sportsman_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" ALTER COLUMN "league_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" DROP CONSTRAINT "FK_6702cbd92992142d61d8d8d67c0"`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" DROP CONSTRAINT "FK_47937695b4e66eec94cda99fbff"`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" ALTER COLUMN "composition_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" ALTER COLUMN "league_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_35aeccfadb95df21011473833f6"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_0c841b3223154cd978d1a05c791"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_a83830d7dcb6014b5732034e19b"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_b1b0db5176687ba92868223aac7"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ALTER COLUMN "sportsman_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ALTER COLUMN "exercise_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ALTER COLUMN "league_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ALTER COLUMN "tableId" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_f2bf76878b91cd3b2469095512d" FOREIGN KEY ("composition_left_id") REFERENCES "composition"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_b1873b5c8ec0ed6551c7dbd6b04" FOREIGN KEY ("composition_right_id") REFERENCES "composition"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_85ce9ec2374d9fd0b5016ff5ae4" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_700c7aea6538afffa9c317a6393" FOREIGN KEY ("tableId") REFERENCES "table_2x2"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_d067a46af3e5c788c1eb2c97665" FOREIGN KEY ("sportsman_left_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_8a68b62be0133ef992cdb64478d" FOREIGN KEY ("sportsman_right_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_5051cad541f47b888057593f3e0" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_460e0380f9baa71928980e3fac5" FOREIGN KEY ("tableId") REFERENCES "table_1x1"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" ADD CONSTRAINT "FK_5e38f296fd8caf6d95bb8b1f4b1" FOREIGN KEY ("sportsman_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" ADD CONSTRAINT "FK_8a92be3c84735c426d7c321ea8b" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" ADD CONSTRAINT "FK_6702cbd92992142d61d8d8d67c0" FOREIGN KEY ("composition_id") REFERENCES "composition"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" ADD CONSTRAINT "FK_47937695b4e66eec94cda99fbff" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_35aeccfadb95df21011473833f6" FOREIGN KEY ("sportsman_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_0c841b3223154cd978d1a05c791" FOREIGN KEY ("exercise_id") REFERENCES "exercise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_a83830d7dcb6014b5732034e19b" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_b1b0db5176687ba92868223aac7" FOREIGN KEY ("tableId") REFERENCES "table_exercise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_b1b0db5176687ba92868223aac7"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_a83830d7dcb6014b5732034e19b"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_0c841b3223154cd978d1a05c791"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_35aeccfadb95df21011473833f6"`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" DROP CONSTRAINT "FK_47937695b4e66eec94cda99fbff"`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" DROP CONSTRAINT "FK_6702cbd92992142d61d8d8d67c0"`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" DROP CONSTRAINT "FK_8a92be3c84735c426d7c321ea8b"`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" DROP CONSTRAINT "FK_5e38f296fd8caf6d95bb8b1f4b1"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_460e0380f9baa71928980e3fac5"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_5051cad541f47b888057593f3e0"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_8a68b62be0133ef992cdb64478d"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" DROP CONSTRAINT "FK_d067a46af3e5c788c1eb2c97665"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_700c7aea6538afffa9c317a6393"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_85ce9ec2374d9fd0b5016ff5ae4"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_b1873b5c8ec0ed6551c7dbd6b04"`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" DROP CONSTRAINT "FK_f2bf76878b91cd3b2469095512d"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ALTER COLUMN "tableId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ALTER COLUMN "league_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ALTER COLUMN "exercise_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ALTER COLUMN "sportsman_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_b1b0db5176687ba92868223aac7" FOREIGN KEY ("tableId") REFERENCES "table_exercise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_a83830d7dcb6014b5732034e19b" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_0c841b3223154cd978d1a05c791" FOREIGN KEY ("exercise_id") REFERENCES "exercise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_35aeccfadb95df21011473833f6" FOREIGN KEY ("sportsman_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" ALTER COLUMN "league_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" ALTER COLUMN "composition_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" ADD CONSTRAINT "FK_47937695b4e66eec94cda99fbff" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" ADD CONSTRAINT "FK_6702cbd92992142d61d8d8d67c0" FOREIGN KEY ("composition_id") REFERENCES "composition"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" ALTER COLUMN "league_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" ALTER COLUMN "sportsman_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" ADD CONSTRAINT "FK_8a92be3c84735c426d7c321ea8b" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" ADD CONSTRAINT "FK_5e38f296fd8caf6d95bb8b1f4b1" FOREIGN KEY ("sportsman_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ALTER COLUMN "tableId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ALTER COLUMN "league_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ALTER COLUMN "sportsman_right_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ALTER COLUMN "sportsman_left_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_460e0380f9baa71928980e3fac5" FOREIGN KEY ("tableId") REFERENCES "table_1x1"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_5051cad541f47b888057593f3e0" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_8a68b62be0133ef992cdb64478d" FOREIGN KEY ("sportsman_right_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_1x1" ADD CONSTRAINT "FK_d067a46af3e5c788c1eb2c97665" FOREIGN KEY ("sportsman_left_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ALTER COLUMN "tableId" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ALTER COLUMN "league_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ALTER COLUMN "composition_right_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ALTER COLUMN "composition_left_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_700c7aea6538afffa9c317a6393" FOREIGN KEY ("tableId") REFERENCES "table_2x2"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_85ce9ec2374d9fd0b5016ff5ae4" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_b1873b5c8ec0ed6551c7dbd6b04" FOREIGN KEY ("composition_right_id") REFERENCES "composition"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_match_2x2" ADD CONSTRAINT "FK_f2bf76878b91cd3b2469095512d" FOREIGN KEY ("composition_left_id") REFERENCES "composition"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
