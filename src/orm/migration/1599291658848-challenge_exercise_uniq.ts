import {MigrationInterface, QueryRunner} from "typeorm";

export class challengeExerciseUniq1599291658848 implements MigrationInterface {
    name = 'challengeExerciseUniq1599291658848'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "UQ_410af624378ff4f150a3cb463e3" UNIQUE ("sportsman_id", "exercise_id", "league_id", "tableId")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "UQ_410af624378ff4f150a3cb463e3"`);
    }

}
