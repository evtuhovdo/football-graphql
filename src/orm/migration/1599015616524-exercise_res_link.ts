import {MigrationInterface, QueryRunner} from "typeorm";

export class exerciseResLink1599015616524 implements MigrationInterface {
    name = 'exerciseResLink1599015616524'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD "exercise_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP CONSTRAINT "FK_0e4b671af8771d58d6cf331beb8"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP CONSTRAINT "FK_fe150b2f898f3aa675f84a7262f"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP COLUMN "points"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD "points" integer NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ALTER COLUMN "position" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ALTER COLUMN "sportsman_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ALTER COLUMN "league_id" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD CONSTRAINT "FK_0e4b671af8771d58d6cf331beb8" FOREIGN KEY ("sportsman_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD CONSTRAINT "FK_fe150b2f898f3aa675f84a7262f" FOREIGN KEY ("league_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD CONSTRAINT "FK_fa1803a387442fff988f446200c" FOREIGN KEY ("exercise_id") REFERENCES "exercise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP CONSTRAINT "FK_fa1803a387442fff988f446200c"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP CONSTRAINT "FK_fe150b2f898f3aa675f84a7262f"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP CONSTRAINT "FK_0e4b671af8771d58d6cf331beb8"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ALTER COLUMN "league_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ALTER COLUMN "sportsman_id" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ALTER COLUMN "position" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP COLUMN "points"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD "points" numeric NOT NULL DEFAULT 0`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD CONSTRAINT "FK_fe150b2f898f3aa675f84a7262f" FOREIGN KEY ("league_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD CONSTRAINT "FK_0e4b671af8771d58d6cf331beb8" FOREIGN KEY ("sportsman_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP COLUMN "exercise_id"`);
    }

}
