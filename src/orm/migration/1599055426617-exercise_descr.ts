import {MigrationInterface, QueryRunner} from "typeorm";

export class exerciseDescr1599055426617 implements MigrationInterface {
    name = 'exerciseDescr1599055426617'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercise" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "exercise" ADD "description" text NOT NULL DEFAULT ''`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercise" DROP COLUMN "description"`);
        await queryRunner.query(`ALTER TABLE "exercise" ADD "description" character varying(255) NOT NULL`);
    }

}
