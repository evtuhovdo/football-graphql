import {MigrationInterface, QueryRunner} from "typeorm";

export class exerciseUnitNotNull1599014298857 implements MigrationInterface {
    name = 'exerciseUnitNotNull1599014298857'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercise" ALTER COLUMN "unit" SET NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercise" ALTER COLUMN "unit" DROP NOT NULL`);
    }

}
