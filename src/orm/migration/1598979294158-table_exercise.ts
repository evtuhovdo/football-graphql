import {MigrationInterface, QueryRunner} from "typeorm";

export class tableExercise1598979294158 implements MigrationInterface {
    name = 'tableExercise1598979294158'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "table_exercise" ("id" uuid NOT NULL DEFAULT uuid_generate_v4(), "number" integer NOT NULL DEFAULT nextval('standings_tables_number_seq'::regclass), "trainer_id" integer, "league_id" integer, CONSTRAINT "PK_fea406fc8d7e76cdae5477298b1" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "table_exercise_sportsmans_identity" ("tableExerciseId" uuid NOT NULL, "identityId" integer NOT NULL, CONSTRAINT "PK_b37f4358a05a9352f2c87df555f" PRIMARY KEY ("tableExerciseId", "identityId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_1cccf3f7151ebf7e837c6c80aa" ON "table_exercise_sportsmans_identity" ("tableExerciseId") `);
        await queryRunner.query(`CREATE INDEX "IDX_b1b6a1537934a2686f86ef9fe2" ON "table_exercise_sportsmans_identity" ("identityId") `);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD "tableId" uuid`);
        await queryRunner.query(`ALTER TABLE "table_exercise" ADD CONSTRAINT "FK_fbb9dc5163bca7110f350163382" FOREIGN KEY ("trainer_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "table_exercise" ADD CONSTRAINT "FK_d34c1e871a3706c487c30ced2c4" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" ADD CONSTRAINT "FK_b1b0db5176687ba92868223aac7" FOREIGN KEY ("tableId") REFERENCES "table_exercise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "table_exercise_sportsmans_identity" ADD CONSTRAINT "FK_1cccf3f7151ebf7e837c6c80aad" FOREIGN KEY ("tableExerciseId") REFERENCES "table_exercise"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "table_exercise_sportsmans_identity" ADD CONSTRAINT "FK_b1b6a1537934a2686f86ef9fe29" FOREIGN KEY ("identityId") REFERENCES "identity"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "table_exercise_sportsmans_identity" DROP CONSTRAINT "FK_b1b6a1537934a2686f86ef9fe29"`);
        await queryRunner.query(`ALTER TABLE "table_exercise_sportsmans_identity" DROP CONSTRAINT "FK_1cccf3f7151ebf7e837c6c80aad"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP CONSTRAINT "FK_b1b0db5176687ba92868223aac7"`);
        await queryRunner.query(`ALTER TABLE "table_exercise" DROP CONSTRAINT "FK_d34c1e871a3706c487c30ced2c4"`);
        await queryRunner.query(`ALTER TABLE "table_exercise" DROP CONSTRAINT "FK_fbb9dc5163bca7110f350163382"`);
        await queryRunner.query(`ALTER TABLE "challenge_exercise" DROP COLUMN "tableId"`);
        await queryRunner.query(`DROP INDEX "IDX_b1b6a1537934a2686f86ef9fe2"`);
        await queryRunner.query(`DROP INDEX "IDX_1cccf3f7151ebf7e837c6c80aa"`);
        await queryRunner.query(`DROP TABLE "table_exercise_sportsmans_identity"`);
        await queryRunner.query(`DROP TABLE "table_exercise"`);
    }

}
