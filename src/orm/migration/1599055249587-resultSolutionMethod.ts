import {MigrationInterface, QueryRunner} from "typeorm";

export class resultSolutionMethod1599055249587 implements MigrationInterface {
    name = 'resultSolutionMethod1599055249587'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercise" DROP COLUMN "resultSolutionMethod"`);
        await queryRunner.query(`CREATE TYPE "exercise_resultsolutionmethod_enum" AS ENUM('BEST_MIN', 'BEST_MAX')`);
        await queryRunner.query(`ALTER TABLE "exercise" ADD "resultSolutionMethod" "exercise_resultsolutionmethod_enum" NOT NULL DEFAULT 'BEST_MAX'`);
        await queryRunner.query(`CREATE INDEX "IDX_2cd43102f4e307613b87076250" ON "exercise" ("resultSolutionMethod") `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "IDX_2cd43102f4e307613b87076250"`);
        await queryRunner.query(`ALTER TABLE "exercise" DROP COLUMN "resultSolutionMethod"`);
        await queryRunner.query(`DROP TYPE "exercise_resultsolutionmethod_enum"`);
        await queryRunner.query(`ALTER TABLE "exercise" ADD "resultSolutionMethod" character varying NOT NULL`);
    }

}
