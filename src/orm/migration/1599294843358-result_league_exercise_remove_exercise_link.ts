import {MigrationInterface, QueryRunner} from "typeorm";

export class resultLeagueExerciseRemoveExerciseLink1599294843358 implements MigrationInterface {
    name = 'resultLeagueExerciseRemoveExerciseLink1599294843358'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP CONSTRAINT "FK_fa1803a387442fff988f446200c"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP COLUMN "exercise_id"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD "exercise_id" integer NOT NULL`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD CONSTRAINT "FK_fa1803a387442fff988f446200c" FOREIGN KEY ("exercise_id") REFERENCES "exercise"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
