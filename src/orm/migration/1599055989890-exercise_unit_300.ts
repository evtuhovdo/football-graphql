import {MigrationInterface, QueryRunner} from "typeorm";

export class exerciseUnit3001599055989890 implements MigrationInterface {
    name = 'exerciseUnit3001599055989890'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercise" DROP COLUMN "unit"`);
        await queryRunner.query(`ALTER TABLE "exercise" ADD "unit" character varying(300) NOT NULL DEFAULT ''`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercise" DROP COLUMN "unit"`);
        await queryRunner.query(`ALTER TABLE "exercise" ADD "unit" character varying(20) NOT NULL`);
    }

}
