import {MigrationInterface, QueryRunner} from "typeorm";

export class exerciseVideoLink1599014927934 implements MigrationInterface {
    name = 'exerciseVideoLink1599014927934'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercise" ADD "exercise_video_id" integer`);
        await queryRunner.query(`ALTER TABLE "exercise" ADD CONSTRAINT "FK_b4b4a7a0b0fd74b77548e59c257" FOREIGN KEY ("exercise_video_id") REFERENCES "exercise_videos"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "exercise" DROP CONSTRAINT "FK_b4b4a7a0b0fd74b77548e59c257"`);
        await queryRunner.query(`ALTER TABLE "exercise" DROP COLUMN "exercise_video_id"`);
    }

}
