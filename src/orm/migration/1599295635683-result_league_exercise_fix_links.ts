import {MigrationInterface, QueryRunner} from "typeorm";

export class resultLeagueExerciseFixLinks1599295635683 implements MigrationInterface {
    name = 'resultLeagueExerciseFixLinks1599295635683'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP CONSTRAINT "FK_fe150b2f898f3aa675f84a7262f"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD CONSTRAINT "FK_fe150b2f898f3aa675f84a7262f" FOREIGN KEY ("league_id") REFERENCES "league"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP CONSTRAINT "FK_fe150b2f898f3aa675f84a7262f"`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD CONSTRAINT "FK_fe150b2f898f3aa675f84a7262f" FOREIGN KEY ("league_id") REFERENCES "identity"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

}
