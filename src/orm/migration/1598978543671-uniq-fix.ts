import {MigrationInterface, QueryRunner} from "typeorm";

export class uniqFix1598978543671 implements MigrationInterface {
    name = 'uniqFix1598978543671'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "identity" DROP COLUMN "come_to_event"`);
        await queryRunner.query(`UPDATE "identity" SET "third_name" = '' WHERE "third_name" IS NULL`);
        await queryRunner.query(`ALTER TABLE "identity" ALTER COLUMN "third_name" SET DEFAULT ''`);
        await queryRunner.query(`ALTER TABLE "identity" ALTER COLUMN "third_name" SET NOT NULL`);
        await queryRunner.query(`ALTER TABLE "identity" DROP CONSTRAINT "UQ_2d515dff2427153b10726f322e0"`);
        await queryRunner.query(`ALTER TABLE "identity" ADD CONSTRAINT "UQ_c4370fe7ca5cd6308d73290291b" UNIQUE ("number_in_team", "team_id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "identity" DROP CONSTRAINT "UQ_c4370fe7ca5cd6308d73290291b"`);
        await queryRunner.query(`ALTER TABLE "identity" ADD CONSTRAINT "UQ_2d515dff2427153b10726f322e0" UNIQUE ("number_in_team")`);
        await queryRunner.query(`ALTER TABLE "identity" ALTER COLUMN "third_name" DROP DEFAULT`);
        await queryRunner.query(`ALTER TABLE "identity" ALTER COLUMN "third_name" DROP NOT NULL`);
        await queryRunner.query(`ALTER TABLE "identity" ADD "come_to_event" boolean`);
    }

}
