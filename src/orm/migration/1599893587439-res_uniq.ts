import {MigrationInterface, QueryRunner} from "typeorm";

export class resUniq1599893587439 implements MigrationInterface {
    name = 'resUniq1599893587439'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "result_league_1x1" ADD CONSTRAINT "UQ_87a355112fc1bb5147b1da97579" UNIQUE ("sportsman_id", "league_id")`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" ADD CONSTRAINT "UQ_c7bdd05403ac10e5436e8aacbdc" UNIQUE ("composition_id", "league_id")`);
        await queryRunner.query(`ALTER TABLE "result_league_exercise" ADD CONSTRAINT "UQ_f92138dacc256527a9315622eda" UNIQUE ("sportsman_id", "league_id")`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "result_league_exercise" DROP CONSTRAINT "UQ_f92138dacc256527a9315622eda"`);
        await queryRunner.query(`ALTER TABLE "result_league_2x2" DROP CONSTRAINT "UQ_c7bdd05403ac10e5436e8aacbdc"`);
        await queryRunner.query(`ALTER TABLE "result_league_1x1" DROP CONSTRAINT "UQ_87a355112fc1bb5147b1da97579"`);
    }

}
