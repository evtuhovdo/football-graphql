import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import League from '../League';
import Sportsman from '../Sportsman';

@Unique(['sportsman', 'league'])
@Entity('result_league_exercise')
class LeagueExerciseResult {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @ManyToOne(() => Sportsman, { eager: true, nullable: false })
  @JoinColumn({
    name: 'sportsman_id',
  })
  sportsman: Promise<Sportsman>;

  @ManyToOne(() => League, { eager: true, nullable: false })
  @JoinColumn({
    name: 'league_id',
  })
  league: Promise<League>;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
  })
  points: number | never = 0;

  @Column({
    nullable: false,
    type: 'integer',
  })
  position: number | null = 0;
}

export default LeagueExerciseResult;
