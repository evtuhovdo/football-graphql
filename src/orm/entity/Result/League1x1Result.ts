import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn, Unique,
} from 'typeorm';
import League from '../League';
import Sportsman from '../Sportsman';

@Unique(['sportsman', 'league'])
@Entity('result_league_1x1', {
  orderBy: {
    position: "ASC",
  }
})
class League1x1Result {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @ManyToOne(() => Sportsman, { eager: true, nullable: false })
  @JoinColumn({
    name: 'sportsman_id',
  })
  sportsman: Promise<Sportsman>;

  @ManyToOne(() => League, { eager: true, nullable: false })
  @JoinColumn({
    name: 'league_id',
  })
  league: Promise<League>;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
  })
  points: number | never = 0;

  @Column({
    nullable: false,
    type: 'integer',
    default: 0,
  })
  position: number | never = 0;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
    name: 'games_count',
  })
  gamesCount: number | never = 0;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
    name: 'wins_count',
  })
  winsCount: number | never = 0;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
    name: 'draw_count',
  })
  drawCount: number | never = 0;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
    name: 'loss_count',
  })
  lossCount: number | never = 0;
}

export default League1x1Result;
