import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn, Unique,
} from 'typeorm';
import Composition from '../Composition/Composition';
import League from '../League';

@Unique(['composition', 'league'])
@Entity('result_league_2x2', {
  orderBy: {
    position: 'ASC',
  },
})
class League2x2Result {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @ManyToOne(() => Composition, { eager: true, nullable: false })
  @JoinColumn({
    name: 'composition_id',
  })
  composition: Promise<Composition>;

  @ManyToOne(() => League, { eager: true, nullable: false })
  @JoinColumn({
    name: 'league_id',
  })
  league: Promise<League>;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
  })
  points: number | never = 0;

  @Column({
    nullable: false,
    type: 'integer',
    default: 0,
  })
  position: number | never = 0;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
    name: 'games_count',
  })
  gamesCount: number | never = 0;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
    name: 'wins_count',
  })
  winsCount: number | never = 0;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
    name: 'draw_count',
  })
  drawCount: number | never = 0;

  @Column({
    nullable: false,
    default: 0,
    type: 'integer',
    name: 'loss_count',
  })
  lossCount: number | never = 0;
}

export default League2x2Result;
