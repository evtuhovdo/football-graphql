import {
  Column,
  ManyToOne,
  Entity,
  PrimaryGeneratedColumn,
  Index,
} from 'typeorm';

import Sportsman from './Sportsman';

export enum SportsmansPersonalParamNameEnum {
  FAT_PERCENT = 'FAT_PERCENT',
  HEIGHT = 'HEIGHT',
  WEIGHT = 'WEIGHT',
  BICEPS = 'BICEPS',
  CHEST = 'CHEST',
  WAIST = 'WAIST',
  HIP = 'HIP',
  SHIN = 'SHIN',
}

@Entity('sportsmans_personal_params')
class SportsmansPersonalParams {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('numeric', {
    nullable: false,
    name: 'value',
  })
  value: number;

  @Column({
    nullable: false,
    type: 'enum',
    enum: SportsmansPersonalParamNameEnum,
    name: 'name',
  })
  @Index()
  name: string;

  @Column('timestamp without time zone', {
    nullable: false,
    default: () => 'NOW()',
    name: 'date',
  })
  date: Date;

  @ManyToOne(type => Sportsman, {
    nullable: false,
  })
  sportsman: Sportsman;
}

export default SportsmansPersonalParams;
