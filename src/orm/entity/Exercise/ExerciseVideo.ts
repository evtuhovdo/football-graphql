import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  Check,
  OneToMany,
  JoinTable,
} from 'typeorm';
import Exercise from './Exercise';

@Entity('exercise_videos')
@Check(`("video_url" IS NOT NULL OR "youtube_video_id" IS NOT NULL) AND ("video_url" <> '' OR "youtube_video_id" <> '')`)
@Check(`"video_url" IS NULL OR "youtube_video_id" IS NULL`)
class ExerciseVideo {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'video_url',
  })
  videoUrl: string;

  @Column('character varying', {
    nullable: true,
    length: 20,
    name: 'youtube_video_id',
  })
  youtubeVideoId: string;

  @Column('character varying', {
    nullable: false,
    length: 6,
    name: 'color',
    default: 'F05E71'
  })
  color: string;

  @Column('character varying', {
    nullable: false,
    length: 3000,
    name: 'description',
  })
  description: string;

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'name',
  })
  name: string;

  @Column('smallint', {
    nullable: false,
    name: 'order',
    default: 0,
  })
  order: number;

  @OneToMany(() => Exercise, exercise => exercise.video)
  @JoinTable()
  exercises: Promise<Exercise[]>;
}

export default ExerciseVideo;
