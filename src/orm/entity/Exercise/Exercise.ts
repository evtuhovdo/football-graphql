import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { LeagueTypeEnum } from '../League';
import ExerciseVideo from './ExerciseVideo';

export enum ExerciseResolutionMethodEnum {
  BEST_MIN = 'BEST_MIN',
  BEST_MAX = 'BEST_MAX',
}

@Entity('exercise')
@Index('exercise_code_key', ['code'], { unique: true })
class Exercise {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'name',
  })
  name: string;

  @Column('character varying', {
    nullable: false,
    length: 300,
    name: 'unit',
    default: '',
  })
  unit: string = '';

  @Column('character varying', {
    nullable: false,
    unique: true,
    length: 255,
    name: 'code',
  })
  code: string;

  @Column({
    nullable: false,
    type: 'enum',
    enum: ExerciseResolutionMethodEnum,
    default: ExerciseResolutionMethodEnum.BEST_MAX,
  })
  @Index()
  resultSolutionMethod: ExerciseResolutionMethodEnum = ExerciseResolutionMethodEnum.BEST_MAX;

  @Column('integer', {
    nullable: false,
  })
  numberOfAttempts: number;

  @Column('integer', {
    nullable: false,
  })
  timeToAttempt: number;

  @Column('text', {
    nullable: false,
    name: 'description',
    default: '',
  })
  description: string;

  @ManyToOne(() => ExerciseVideo, { nullable: true })
  @JoinColumn({
    name: 'exercise_video_id',
  })
  video: Promise<ExerciseVideo>;
}

export default Exercise;
