import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  ManyToOne, JoinColumn, Check,
} from 'typeorm';
import Composition from '../Composition/Composition';
import League from '../League';
import Table2x2 from '../Table/Table2x2';

@Entity('challenge_match_2x2')
@Check(`"composition_left_id" <> "composition_right_id"`)
@Check(`"left_score" >= 0`)
@Check(`"right_score" >= 0`)
class Match2x2 {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
    unsigned: true,
  })
  id: number;

  @Column('date', {
    nullable: false,
  })
  date: Date;

  @Column('integer', {
    nullable: false,
    name: 'left_score',
  })
  leftScore: number;

  @Column('integer', {
    nullable: false,
    name: 'right_score',
  })
  rightScore: number;

  @ManyToOne(() => Composition, { eager: true, nullable: false })
  @JoinColumn({
    name: 'composition_left_id',
  })
  compositionLeft: Promise<Composition>;

  @ManyToOne(() => Composition, { eager: true, nullable: false })
  @JoinColumn({
    name: 'composition_right_id',
  })
  compositionRight: Promise<Composition>;

  @ManyToOne(() => League, { eager: true, nullable: false })
  @JoinColumn({
    name: 'league_id',
  })
  league: Promise<League>;

  @Column('boolean', {
    nullable: false,
    default: false,
    name: 'saved_in_rating',
  })
  savedInRating: boolean = false;

  @ManyToOne(type => Table2x2, table => table.matches, { nullable: false })
  @JoinColumn({
    name: 'table_id',
  })
  table: Promise<Table2x2>;
}

export default Match2x2;
