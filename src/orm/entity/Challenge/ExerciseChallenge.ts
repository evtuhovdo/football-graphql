import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  ManyToOne, JoinColumn, Unique,
} from 'typeorm';
import Exercise from '../Exercise/Exercise';
import League from '../League';
import Sportsman from '../Sportsman';
import TableExercise from '../Table/TableExercise';

@Unique(['sportsman', 'exercise', 'league', 'table'])
@Entity('challenge_exercise')
class ExerciseChallenge {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
    unsigned: true,
  })
  id: number;

  @Column('date', {
    nullable: false,
  })
  date: Date;

  @Column('decimal', {
    nullable: false,
    default: 0,
    name: 'result_value',
  })
  resultValue: number;

  @ManyToOne(() => Sportsman, { eager: true, nullable: false })
  @JoinColumn({
    name: 'sportsman_id',
  })
  sportsman: Promise<Sportsman>;

  @ManyToOne(() => Exercise, { eager: true, nullable: false })
  @JoinColumn({
    name: 'exercise_id',
  })
  exercise: Promise<Exercise>;

  @ManyToOne(() => League, { eager: true, nullable: false })
  @JoinColumn({
    name: 'league_id',
  })
  league: Promise<League>;

  @Column('boolean', {
    nullable: false,
    default: false,
    name: 'saved_in_rating',
  })
  savedInRating: boolean = false;

  @ManyToOne(type => TableExercise, table => table.challenges, { nullable: false })
  @JoinColumn({
    name: 'table_id',
  })
  table: Promise<TableExercise>;
}

export default ExerciseChallenge;
