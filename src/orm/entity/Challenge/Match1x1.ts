import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  ManyToOne, JoinColumn, Check,
} from 'typeorm';
import League from '../League';
import Sportsman from '../Sportsman';
import Table1x1 from '../Table/Table1x1';
import Trainer from '../Trainer';

@Entity('challenge_match_1x1')
@Check(`"sportsman_left_id" <> "sportsman_right_id"`)
@Check(`"left_score" >= 0`)
@Check(`"right_score" >= 0`)
class Match1x1 {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
    unsigned: true,
  })
  id: number;

  @Column('date', {
    nullable: false,
  })
  date: Date;

  @Column('integer', {
    nullable: false,
    name: 'left_score',
  })
  leftScore: number;

  @Column('integer', {
    nullable: false,
    name: 'right_score',
  })
  rightScore: number;

  @ManyToOne(() => Sportsman, { eager: true, nullable: false })
  @JoinColumn({
    name: 'sportsman_left_id',
  })
  sportsmanLeft: Promise<Sportsman>;

  @ManyToOne(() => Sportsman, { eager: true, nullable: false })
  @JoinColumn({
    name: 'sportsman_right_id',
  })
  sportsmanRight: Promise<Sportsman>;

  @ManyToOne(() => League, { eager: true, nullable: false })
  @JoinColumn({
    name: 'league_id',
  })
  league: Promise<League>;

  @Column('boolean', {
    nullable: false,
    default: false,
    name: 'saved_in_rating',
  })
  savedInRating: boolean = false;

  @ManyToOne(type => Table1x1, table => table.matches, { nullable: false })
  @JoinColumn({
    name: 'table_id',
  })
  table: Promise<Table1x1>;
}

export default Match1x1;
