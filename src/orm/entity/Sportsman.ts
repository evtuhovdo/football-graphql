import {
  Column,
  ChildEntity,
  ManyToOne,
  JoinColumn,
  Index,
  ManyToMany,
  Unique,
} from 'typeorm';
import Composition from './Composition/Composition';
import Table1x1 from './Table/Table1x1';
import Team from './Team';
import Identity from './Identity';


@Unique(['numberInTeam', 'team'])
@ChildEntity()
class Sportsman extends Identity {
  @Column('smallint', {
    nullable: false,
    name: 'number_in_team',
  })
  @Index()
  numberInTeam: number;

  @ManyToOne(() => Team)
  @JoinColumn({
    name: 'team_id',
  })
  team: Promise<Team>;

  @Column()
  rating: number;

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'field_position',
  })
  fieldPosition: string;

  @ManyToMany(() => Composition, (composition) => composition.sportsmans)
  compositions: Promise<Composition[]>;

  @ManyToMany(
    () => Table1x1,
    (table1x1) => table1x1.sportsmans,
  )
  tables1x1: Promise<Table1x1[]>;
}

export default Sportsman;
