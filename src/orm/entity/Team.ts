import {
  Column,
  Entity, JoinTable, OneToMany, ManyToOne,
  PrimaryGeneratedColumn, ManyToMany,
} from 'typeorm';
import Composition from './Composition/Composition';
import Trainer from './Trainer';
import Sportsman from './Sportsman';

@Entity('team')
class Team {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'name',
  })
  name: string;

  @ManyToOne(type => Trainer, trainer => trainer.id)
  trainer: Promise<Trainer>;

  @OneToMany(type => Sportsman, sportsman => sportsman.team)
  @JoinTable()
  sportsmans: Promise<Sportsman[]>;

  @OneToMany(() => Composition, (composition) => composition.team)
  @JoinTable()
  compositions: Promise<Composition[]>;
}

export default Team;
