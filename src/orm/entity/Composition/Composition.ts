import {
  PrimaryGeneratedColumn,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  Check,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import Sportsman from '../Sportsman';
import Table2x2 from '../Table/Table2x2';
import Team from '../Team';

export enum CompositionEnum {
  COMPOSITION_2x2 = 'COMPOSITION_2x2',
}

@Entity('composition')
@Check(`"min_capacity" <= "max_capacity"`)
class Composition {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    nullable: false,
    type: 'enum',
    enum: CompositionEnum,
    default: CompositionEnum.COMPOSITION_2x2,
  })
  type: CompositionEnum;

  @Column('character varying', {
    nullable: true,
    length: 100,
  })
  name: string | null;

  @Column({
    type: 'integer',
    nullable: false,
    name: 'min_capacity',
  })
  minCapacity: number;

  @Column({
    type: 'integer',
    nullable: false,
    name: 'max_capacity',
  })
  maxCapacity: number;

  @ManyToMany(
    () => Sportsman,
    (sportsman) => sportsman.compositions,
    { eager: true },
  )
  @JoinTable()
  sportsmans: Promise<Sportsman[]>;

  @ManyToOne(() => Team)
  @JoinColumn({
    name: 'team_id',
  })
  team: Promise<Team>;

  @ManyToMany(
    () => Table2x2,
    (table2x2) => table2x2.compositions,
  )
  tables2x2: Promise<Table2x2[]>;
}

export default Composition;
