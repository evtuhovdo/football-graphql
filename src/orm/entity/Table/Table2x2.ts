import {
  Column,
  Entity, Generated,
  JoinColumn, JoinTable,
  ManyToMany,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Match2x2 from '../Challenge/Match2x2';
import Composition from '../Composition/Composition';
import League from '../League';
import Sportsman from '../Sportsman';
import Trainer from '../Trainer';

@Entity('table_2x2')
class Table2x2 {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column('int', {
    nullable: false,
    default: () => 'nextval(\'standings_tables_number_seq\'::regclass)',
  })
  number: number;

  @ManyToOne(() => Trainer, { eager: true })
  @JoinColumn({
    name: 'trainer_id',
  })
  trainer: Trainer;

  @ManyToOne(() => League)
  @JoinColumn({
    name: 'league_id',
  })
  league: Promise<League>;

  @ManyToMany(() => Composition, (composition) => composition.tables2x2)
  @JoinTable()
  compositions: Promise<Sportsman[]>;

  @OneToMany(() => Match2x2, match => match.table)
  @JoinTable()
  matches: Promise<Match2x2[]>;
}


export default Table2x2;
