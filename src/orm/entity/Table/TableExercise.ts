import {
  Column,
  Entity, Generated,
  JoinColumn, JoinTable,
  ManyToMany,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import ExerciseChallenge from '../Challenge/ExerciseChallenge';
import Match1x1 from '../Challenge/Match1x1';
import Exercise from '../Exercise/Exercise';
import League from '../League';
import Sportsman from '../Sportsman';
import Trainer from '../Trainer';

@Entity('table_exercise')
class TableExercise {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column('int', {
    nullable: false,
    default: () => 'nextval(\'standings_tables_number_seq\'::regclass)',
  })
  number: number;

  @ManyToOne(() => Trainer, { eager: true })
  @JoinColumn({
    name: 'trainer_id',
  })
  trainer: Trainer;

  @ManyToOne(() => League)
  @JoinColumn({
    name: 'league_id',
  })
  league: Promise<League>;

  @ManyToMany(() => Sportsman, (sportsman) => sportsman.tables1x1)
  @JoinTable()
  sportsmans: Promise<Sportsman[]>;

  @OneToMany(() => ExerciseChallenge, match => match.table)
  @JoinTable()
  challenges: Promise<ExerciseChallenge[]>;

  @ManyToMany(() => Exercise)
  @JoinTable({
    name: 'table_exercises_link_exercises'
  })
  exercises: Promise<Exercise[]>;
}


export default TableExercise;
