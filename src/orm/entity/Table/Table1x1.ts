import {
  Column,
  Entity, Generated,
  JoinColumn, JoinTable,
  ManyToMany,
  ManyToOne, OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Match1x1 from '../Challenge/Match1x1';
import League from '../League';
import Sportsman from '../Sportsman';
import Trainer from '../Trainer';

@Entity('table_1x1')
class Table1x1 {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column('int', {
    nullable: false,
    default: () => 'nextval(\'standings_tables_number_seq\'::regclass)',
  })
  number: number;

  @ManyToOne(() => Trainer, { eager: true })
  @JoinColumn({
    name: 'trainer_id',
  })
  trainer: Trainer;

  @ManyToOne(() => League)
  @JoinColumn({
    name: 'league_id',
  })
  league: Promise<League>;

  @ManyToMany(() => Sportsman, (sportsman) => sportsman.tables1x1)
  @JoinTable()
  sportsmans: Promise<Sportsman[]>;

  @OneToMany(() => Match1x1, match => match.table)
  @JoinTable()
  matches: Promise<Match1x1[]>;
}


export default Table1x1;
