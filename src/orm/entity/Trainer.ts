import {
  Column,
  OneToMany,
  ChildEntity, JoinTable,
} from 'typeorm';
import Table1x1 from './Table/Table1x1';
import Table2x2 from './Table/Table2x2';
import TableExercise from './Table/TableExercise';
import Team from './Team';
import Identity from './Identity';

@ChildEntity()
class Trainer extends Identity {
  @Column('character varying', {
    nullable: true,
    length: 100,
    name: 'place_of_work',
  })
  placeOfWork: string;

  @OneToMany(() => Team, team => team.trainer)
  @JoinTable()
  teams: Promise<Team[]>;

  @OneToMany(() => Table1x1, table1x1 => table1x1.trainer)
  @JoinTable()
  tables1x1: Promise<Table1x1[]>;

  @OneToMany(() => Table2x2, table2x2 => table2x2.trainer)
  @JoinTable()
  tables2x2: Promise<Table2x2[]>;

  @OneToMany(() => TableExercise, tableExercise => tableExercise.trainer)
  @JoinTable()
  tablesExercise: Promise<TableExercise[]>;
}

export default Trainer;
