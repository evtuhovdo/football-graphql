import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  TableInheritance,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Role from './Role';

@Entity('identity')
@Index('identity_email_key', ['email'], { unique: true })
@Index('identity_phone_key', ['phone'], { unique: true })
@TableInheritance({ column: { type: 'varchar', name: 'type' } })
class Identity {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('character varying', {
    nullable: true,
    length: 255,
    name: 'avatar_url',
  })
  avatarURL: string | null;

  @ManyToOne(() => Role, {
    eager: true,
  })
  @JoinColumn({
    name: 'role_id',
  })
  role: Role;

  @Column('character varying', {
    nullable: false,
    unique: true,
    length: 20,
    name: 'phone',
  })
  phone: string;

  @Column('character varying', {
    nullable: false,
    length: 100,
    name: 'first_name',
  })
  firstName: string | null;

  @Column('character varying', {
    nullable: false,
    length: 100,
    name: 'last_name',
  })
  lastName: string | null;

  @Column('character varying', {
    nullable: false,
    length: 100,
    name: 'third_name',
    default: '',
  })
  thirdName: string;

  @Column('timestamp without time zone', {
    nullable: true,
    name: 'date_of_birth',
  })
  dateOfBirth: Date | null;

  @Column('character varying', {
    nullable: true,
    unique: true,
    length: 50,
    name: 'email',
  })
  email: string | null;

  @Column('character varying', {
    nullable: false,
    length: 64, // sha256
    name: 'password',
  })
  password: string;

  @Column('character varying', {
    nullable: true,
    length: 1,
    name: 'sex',
  })
  sex: string | null;
}

export default Identity;
