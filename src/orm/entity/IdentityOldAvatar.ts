import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Identity from './Identity';

@Entity('identity_old_avatars')
class IdentityOldAvatar {
  @PrimaryGeneratedColumn({
    type: 'integer',
    name: 'id',
  })
  id: number;

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'avatar_url',
  })
  url: string;

  @ManyToOne(() => Identity, {
    eager: true,
  })
  @JoinColumn({
    name: 'user_id',
  })
  user: Identity;
}

export default IdentityOldAvatar;
