import {
  Entity, PrimaryGeneratedColumn, Column, Index, Check, OneToMany, JoinTable,
} from 'typeorm';
import League1x1Result from './Result/League1x1Result';
import League2x2Result from './Result/League2x2Result';
import LeagueExerciseResult from './Result/LeagueExerciseResult';
import Table1x1 from './Table/Table1x1';
import Table2x2 from './Table/Table2x2';

export enum LeagueTypeEnum {
  LEAGUE_1x1 = 'LEAGUE_1x1',
  LEAGUE_2x2 = 'LEAGUE_2x2',
  EXERCISE_LEAGUE = 'EXERCISE_LEAGUE',
}

@Entity()
@Check(`"start_date" <> "end_date"`)
@Check(`"start_date" < "end_date"`)
class League {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('character varying', {
    nullable: false,
    length: 255,
    name: 'name',
  })
  name: string;

  @Column('timestamp without time zone', {
    nullable: false,
    name: 'start_date',
  })
  startDate: Date;

  @Column('timestamp without time zone', {
    nullable: false,
    name: 'end_date',
  })
  endDate: Date;

  @Column({
    nullable: false,
    type: 'enum',
    enum: LeagueTypeEnum,
    name: 'type',
  })
  @Index()
  type: LeagueTypeEnum;

  @OneToMany(() => Table1x1, table1x1 => table1x1.league)
  @JoinTable()
  tables1x1: Promise<Table1x1[]>;

  @OneToMany(() => Table2x2, table2x2 => table2x2.league)
  @JoinTable()
  tables2x2: Promise<Table2x2[]>;

  @OneToMany(() => League1x1Result, league1x1Result => league1x1Result.league)
  @JoinTable()
  leagueResults1x1: Promise<League1x1Result[]>;

  @OneToMany(() => League2x2Result, league2x2Result => league2x2Result.league)
  @JoinTable()
  leagueResults2x2: Promise<League2x2Result[]>;

  @OneToMany(() => LeagueExerciseResult, leagueExerciseResult => leagueExerciseResult.league)
  @JoinTable()
  leagueExerciseResults: Promise<LeagueExerciseResult[]>;
}

export default League;
