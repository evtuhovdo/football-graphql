import OAuth2Server from 'oauth2-server';
import createOauthModel from './createOauthModel';

const accessTokenLifetime: number = 60 * 60 * 24 * 30; // 30 дней

const env: string = process.env.NODE_ENV || 'development';
const isDevelop: boolean = env === 'development';

interface ICreateOauthServer {
  (): Promise<any>;
}

const createOauthServer: ICreateOauthServer = async () => {
  const model = await createOauthModel();

  return new OAuth2Server({
    debug: isDevelop,
    model,
    accessTokenLifetime,
    allowBearerTokensInQueryString: true,
  });
};

export default createOauthServer;
