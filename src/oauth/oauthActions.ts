import { Request, Response } from 'oauth2-server';

interface iOauthArg {
  token: (request: any, response: any) => Promise<void>
}

const refreshToken = (oauth: iOauthArg) => (req: any, res: any) => {
  // TODO: Протестировать
  const request = new Request(req);
  const response = new Response(res);

  return oauth.token(request, response)
    .then((token) => {
      res.json(token);
    }).catch((err) => {
      console.log('err', err);
      res.status(err.code || 500).json(err);
    });
};

const revokeToken = (oauth: iOauthArg) => (req: any, res: any) => {
  // TODO: Протестировать
  const request = new Request(req);
  const response = new Response(res);

  return oauth.token(request, response)
    .then((token) => {
      res.json(token);
    }).catch((err) => {
      console.log('err', err);
      res.status(err.code || 500).json(err);
    });
};

const obtainToken = (oauth: iOauthArg) => (req: any, res: any) => {
  const request = new Request(req);
  const response = new Response(res);

  return oauth.token(request, response)
    .then((token) => {
      res.json(token);
    }).catch((err) => {
      console.log('err', err);
      res.status(err.code || 500).json(err);
    });
};

export {
  refreshToken,
  revokeToken,
  obtainToken,
};
