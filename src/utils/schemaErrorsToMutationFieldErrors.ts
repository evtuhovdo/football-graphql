import map from 'lodash/map';
import { ValidationError } from 'yup';
import { IMutationFieldError } from '../graphql/types/common/MutationFieldError/MutationFieldErrorType';

const schemaErrorsToMutationFieldErrors = (errors: ValidationError): IMutationFieldError[] => {
  return map(errors.inner, (error) => ({
    field: error.path,
    message: error.errors && error.errors.join(' '),
  }));
};

export default schemaErrorsToMutationFieldErrors;
