import password from 'password';

const generatePassword = (words: number = 1, digits: number = 4): string => {
  return password(1) + Math.random().toString().slice(4, 4 + digits);
};

export default generatePassword;
