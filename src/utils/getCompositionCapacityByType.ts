import { CompositionEnum } from '../orm/entity/Composition/Composition';

interface IGetCompositionCapacityByType {
  (type: CompositionEnum): { min: number; max: number }
}

const getCompositionCapacityByType: IGetCompositionCapacityByType = (type) => {
  if (type === CompositionEnum.COMPOSITION_2x2) {
    return {
      min: 2,
      max: 4,
    }
  }

  throw new Error('unknown type');
};

export default getCompositionCapacityByType;
