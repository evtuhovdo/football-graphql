import * as crypto from 'crypto';

const SECRET = 'O9}b2U^85(Q/0[Z';

interface IgetPasswordHash {
  (password: string): string
}

const getPasswordHash: IgetPasswordHash = password => crypto
  .createHmac('sha256', SECRET)
  .update(password)
  .digest('hex');

export default getPasswordHash;
