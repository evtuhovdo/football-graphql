import { EntityManager } from 'typeorm';
import map from 'lodash/map';
import uniq from 'lodash/uniq';

import ExerciseChallenge from '../orm/entity/Challenge/ExerciseChallenge';

import Match1x1 from '../orm/entity/Challenge/Match1x1';
import Match2x2 from '../orm/entity/Challenge/Match2x2';
import League1x1Result from '../orm/entity/Result/League1x1Result';
import League2x2Result from '../orm/entity/Result/League2x2Result';
import LeagueExerciseResult from '../orm/entity/Result/LeagueExerciseResult';

const WIN_POINT_WEIGHT = 3;
const DRAW_POINT_WEIGHT = 1;
const LOSS_POINT_WEIGHT = 0;

export enum SideEnum {
  LEFT = 'LEFT',
  RIGHT = 'RIGHT',
}

interface IMatch {
  leftScore: number;
  rightScore: number;
}

interface IPointFromExercisesResult {
  sportsman_id: number;
  league_id: number;
  sum_points: number;
}

const isWinOf = (side: SideEnum, match: IMatch): boolean => {
  if (side === SideEnum.LEFT) {
    return match.leftScore > match.rightScore;
  }

  if (side === SideEnum.RIGHT) {
    return match.leftScore < match.rightScore;
  }

  return false;
};

const isDrawOf = (match: IMatch): boolean => {
  return match.leftScore === match.rightScore;
};

const isLossOf = (side: SideEnum, match: IMatch): boolean => {
  if (side === SideEnum.LEFT) {
    return match.leftScore < match.rightScore;
  }

  if (side === SideEnum.RIGHT) {
    return match.leftScore > match.rightScore;
  }

  return false;
};

export const runScoreAndRatingCalculation = async (entityManager: EntityManager) => {
  console.log(`START JOB: runScoreAndRatingCalculation ${new Date().toString()}`);
  const matches1x1 = await entityManager.find(Match1x1, {
    where: {
      savedInRating: false,
    },
  });

  const matches2x2 = await entityManager.find(Match2x2, {
    where: {
      savedInRating: false,
    },
  });

  let haveNewExercisesMatches = false;

  await entityManager.transaction(async trEntityManager => {
    const pointFromExercises: IPointFromExercisesResult[] = await trEntityManager.query(`
       SELECT s.sportsman_id, s.league_id, CAST (SUM(s.point_from_exercise) AS INTEGER) as sum_points FROM (
          SELECT sportsman_id, league_id, DENSE_RANK () OVER (
              PARTITION BY league_id, exercise_id ORDER BY result_value ASC
          ) point_from_exercise FROM challenge_exercise  WHERE saved_in_rating = false
      ) s
      GROUP BY sportsman_id, league_id
    `);
    if (pointFromExercises && pointFromExercises.length) {
      haveNewExercisesMatches = true;
      console.log(`Start calculation score for ${pointFromExercises.length} exercise result.`);

      const newResult = map(pointFromExercises, item => {
        return {
          sportsman: item.sportsman_id,
          league: item.league_id,
          points: item.sum_points,
          position: 0,
        }
      });

      const leaguesIds = uniq(map(pointFromExercises, item => {
        return item.league_id;
      }));

      await trEntityManager.query(`
        DELETE FROM result_league_exercise WHERE result_league_exercise.league_id = ANY ($1)
      `, [leaguesIds]);

      await trEntityManager.query(`
        UPDATE challenge_exercise SET saved_in_rating = TRUE WHERE saved_in_rating = false
      `);

      await trEntityManager.createQueryBuilder()
        .insert()
        .into(LeagueExerciseResult)
        // @ts-ignore
        .values(newResult)
        .execute();

      console.log(`Score calculated for ${pointFromExercises.length} exercise result.`);
    }
  });

  if (matches1x1.length) {
    console.log(`Start calculation score for ${matches1x1.length} matches 1x1.`);

    map(matches1x1, async (match1x1) => {
      console.log('process match1x1', match1x1.id);
      match1x1.savedInRating = true;
      const sportsmanLeft = await match1x1.sportsmanLeft;
      const sportsmanRight = await match1x1.sportsmanRight;
      const league = await match1x1.league;

      const isDraw = isDrawOf(match1x1);

      const isWinLeft = isWinOf(SideEnum.LEFT, match1x1);
      const isLossLeft = isLossOf(SideEnum.LEFT, match1x1);

      const isWinRight = isWinOf(SideEnum.RIGHT, match1x1);
      const isLossRight = isLossOf(SideEnum.RIGHT, match1x1);

      const resultLEFT1x1InDB = await entityManager.findOne(League1x1Result, {
        where: {
          sportsman: sportsmanLeft,
          league: league,
        },
      });

      const resultRIGHT1x1InDB = await entityManager.findOne(League1x1Result, {
        where: {
          sportsman: sportsmanRight,
          league: league,
        },
      });

      let resultLEFT1x1: League1x1Result;
      if (!resultLEFT1x1InDB) {
        resultLEFT1x1 = new League1x1Result();
        resultLEFT1x1.sportsman = Promise.resolve(sportsmanLeft);
        resultLEFT1x1.league = Promise.resolve(match1x1.league);
      } else {
        resultLEFT1x1 = resultLEFT1x1InDB;
      }

      resultLEFT1x1.gamesCount = resultLEFT1x1.gamesCount ? resultLEFT1x1.gamesCount + 1 : 1;

      if (isWinLeft) {
        resultLEFT1x1.winsCount = resultLEFT1x1.winsCount ? resultLEFT1x1.winsCount + 1 : 1;
        resultLEFT1x1.points = resultLEFT1x1.points ? resultLEFT1x1.points + WIN_POINT_WEIGHT : WIN_POINT_WEIGHT;
      }

      if (isDraw) {
        resultLEFT1x1.drawCount = resultLEFT1x1.drawCount ? resultLEFT1x1.drawCount + 1 : 1;
        resultLEFT1x1.points = resultLEFT1x1.points ? resultLEFT1x1.points + DRAW_POINT_WEIGHT : DRAW_POINT_WEIGHT;
      }

      if (isLossLeft) {
        resultLEFT1x1.lossCount = resultLEFT1x1.lossCount ? resultLEFT1x1.lossCount + 1 : 1;
        resultLEFT1x1.points = resultLEFT1x1.points ? resultLEFT1x1.points + LOSS_POINT_WEIGHT : LOSS_POINT_WEIGHT;
      }

      let resultRIGHT1x1: League1x1Result;
      if (!resultRIGHT1x1InDB) {
        resultRIGHT1x1 = new League1x1Result();
        resultRIGHT1x1.sportsman = Promise.resolve(sportsmanRight);
        resultRIGHT1x1.league = Promise.resolve(match1x1.league);
      } else {
        resultRIGHT1x1 = resultRIGHT1x1InDB;
      }

      resultRIGHT1x1.gamesCount = resultRIGHT1x1.gamesCount ? resultRIGHT1x1.gamesCount + 1 : 1;

      if (isWinRight) {
        resultRIGHT1x1.winsCount = resultRIGHT1x1.winsCount ? resultRIGHT1x1.winsCount + 1 : 1;
        resultRIGHT1x1.points = resultRIGHT1x1.points ? resultRIGHT1x1.points + WIN_POINT_WEIGHT : WIN_POINT_WEIGHT;
      }

      if (isDraw) {
        resultRIGHT1x1.drawCount = resultRIGHT1x1.drawCount ? resultRIGHT1x1.drawCount + 1 : 1;
        resultRIGHT1x1.points = resultRIGHT1x1.points ? resultRIGHT1x1.points + DRAW_POINT_WEIGHT : DRAW_POINT_WEIGHT;
      }

      if (isLossRight) {
        resultRIGHT1x1.lossCount = resultRIGHT1x1.lossCount ? resultRIGHT1x1.lossCount + 1 : 1;
        resultRIGHT1x1.points = resultRIGHT1x1.points ? resultRIGHT1x1.points + LOSS_POINT_WEIGHT : LOSS_POINT_WEIGHT;
      }

      await entityManager.transaction(async trEntityManager => {
        await trEntityManager.save([resultRIGHT1x1, resultLEFT1x1, match1x1]);
      });
    });
    console.log(`Score calculated for ${matches1x1.length} matches 1x1.`);
  }

  if (matches2x2.length) {
    console.log(`Start calculation score for ${matches2x2.length} matches 2x2.`);

    for (let prop in matches2x2) {
      const match2x2 = matches2x2[prop];
      console.log('process match2x2', match2x2.id);

      match2x2.savedInRating = true;
      const compositionLeft = await match2x2.compositionLeft;
      const compositionRight = await match2x2.compositionRight;
      const league = await match2x2.league;

      const isDraw = isDrawOf(match2x2);

      const isWinLeft = isWinOf(SideEnum.LEFT, match2x2);
      const isLossLeft = isLossOf(SideEnum.LEFT, match2x2);

      const isWinRight = isWinOf(SideEnum.RIGHT, match2x2);
      const isLossRight = isLossOf(SideEnum.RIGHT, match2x2);

      const resultLEFT2x2InDB = await entityManager.findOne(League2x2Result, {
        where: {
          composition: compositionLeft,
          league: league,
        },
      });

      const resultRIGHT2x2InDB = await entityManager.findOne(League2x2Result, {
        where: {
          composition: compositionRight,
          league: league,
        },
      });

      let resultLEFT2x2: League2x2Result | null = null;
      if (!resultLEFT2x2InDB) {
        resultLEFT2x2 = new League2x2Result();
        resultLEFT2x2.composition = Promise.resolve(compositionLeft);
        resultLEFT2x2.league = Promise.resolve(match2x2.league);
      } else {
        resultLEFT2x2 = resultLEFT2x2InDB;
      }

      resultLEFT2x2.gamesCount = resultLEFT2x2.gamesCount ? resultLEFT2x2.gamesCount + 1 : 1;

      if (isWinLeft) {
        resultLEFT2x2.winsCount = resultLEFT2x2.winsCount ? resultLEFT2x2.winsCount + 1 : 1;
        resultLEFT2x2.points = resultLEFT2x2.points ? resultLEFT2x2.points + WIN_POINT_WEIGHT : WIN_POINT_WEIGHT;
      }

      if (isDraw) {
        resultLEFT2x2.drawCount = resultLEFT2x2.drawCount ? resultLEFT2x2.drawCount + 1 : 1;
        resultLEFT2x2.points = resultLEFT2x2.points ? resultLEFT2x2.points + DRAW_POINT_WEIGHT : DRAW_POINT_WEIGHT;
      }

      if (isLossLeft) {
        resultLEFT2x2.lossCount = resultLEFT2x2.lossCount ? resultLEFT2x2.lossCount + 1 : 1;
        resultLEFT2x2.points = resultLEFT2x2.points ? resultLEFT2x2.points + LOSS_POINT_WEIGHT : LOSS_POINT_WEIGHT;
      }

      let resultRIGHT2x2: League2x2Result | null = null;
      if (!resultRIGHT2x2InDB) {
        resultRIGHT2x2 = new League2x2Result();
        resultRIGHT2x2.composition = Promise.resolve(compositionRight);
        resultRIGHT2x2.league = Promise.resolve(match2x2.league);
      } else {
        resultRIGHT2x2 = resultRIGHT2x2InDB;
      }

      resultRIGHT2x2.gamesCount = resultRIGHT2x2.gamesCount ? resultRIGHT2x2.gamesCount + 1 : 1;

      if (isWinRight) {
        resultRIGHT2x2.winsCount = resultRIGHT2x2.winsCount ? resultRIGHT2x2.winsCount + 1 : 1;
        resultRIGHT2x2.points = resultRIGHT2x2.points ? resultRIGHT2x2.points + WIN_POINT_WEIGHT : WIN_POINT_WEIGHT;
      }

      if (isDraw) {
        resultRIGHT2x2.drawCount = resultRIGHT2x2.drawCount ? resultRIGHT2x2.drawCount + 1 : 1;
        resultRIGHT2x2.points = resultRIGHT2x2.points ? resultRIGHT2x2.points + DRAW_POINT_WEIGHT : DRAW_POINT_WEIGHT;
      }

      if (isLossRight) {
        resultRIGHT2x2.lossCount = resultRIGHT2x2.lossCount ? resultRIGHT2x2.lossCount + 1 : 1;
        resultRIGHT2x2.points = resultRIGHT2x2.points ? resultRIGHT2x2.points + LOSS_POINT_WEIGHT : LOSS_POINT_WEIGHT;
      }

      await entityManager.transaction(async trEntityManager => {
        await trEntityManager.save(resultRIGHT2x2);
        await trEntityManager.save(resultLEFT2x2);
        await trEntityManager.save(match2x2);
      });
    }

    console.log(`Score calculated for ${matches2x2.length} matches 2x2.`)
  }

  if (matches1x1.length) {
    console.log('start rating calculation for matches1x1');
    await entityManager.query(`
      UPDATE "result_league_1x1" SET "position" = position_to_save FROM (
        SELECT id, DENSE_RANK () OVER ( 
        PARTITION BY league_id ORDER BY points DESC 
        ) position_to_save FROM result_league_1x1
      ) s where result_league_1x1.id = s.id;
    `);
    console.log('end rating calculation for matches1x1');
  }

  if (matches2x2.length) {
    console.log('start rating calculation for matches2x2');
    await entityManager.query(`
      UPDATE "result_league_2x2" SET "position" = position_to_save FROM (
        SELECT id, DENSE_RANK () OVER ( 
        PARTITION BY league_id ORDER BY points DESC 
        ) position_to_save FROm result_league_2x2
      ) s where result_league_2x2.id = s.id;
    `);
    console.log('end rating calculation for matches2x2');
  }

  if (haveNewExercisesMatches) {
    console.log('start rating calculation for result_league_exercise');
    await entityManager.query(`
      UPDATE "result_league_exercise" SET "position" = position_to_save FROM (
        SELECT id, DENSE_RANK () OVER (
            PARTITION BY league_id ORDER BY points DESC
        ) position_to_save FROm result_league_exercise
      ) s where result_league_exercise.id = s.id;
    `);
    console.log('end rating calculation for result_league_exercise');
  }

  console.log('FINISH JOB: runRatingCalculation');
};
