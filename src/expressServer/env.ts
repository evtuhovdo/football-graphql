const fs = require('fs');
const dotenvExpand = require('dotenv-expand');
const dotenv = require('dotenv');

const makeEnv = () => {
  const NODE_ENV: string | undefined = process.env.NODE_ENV;
  if (!NODE_ENV) {
    throw new Error(
      'The NODE_ENV environment variable is required but was not specified.',
    );
  }

  const appDirectory: string = fs.realpathSync(process.cwd());

  // https://github.com/bkeepers/dotenv#what-other-env-files-can-i-use
  const dotEnvFiles: any = [
    `${appDirectory}/.env.${NODE_ENV}.local`,
    `${appDirectory}/.env.${NODE_ENV}`,
    // Don't include `.env.local` for `test` environment
    // since normally you expect tests to produce the same
    // results for everyone
    NODE_ENV !== 'test' && `${appDirectory}/.env.local`,
    `${appDirectory}/.env`,
  ];

  // Load environment variables from .env* files. Suppress warnings using silent
  // if this file is missing. dotenv will never modify any environment variables
  // that have already been set.  Variable expansion is supported in .env files.
  // https://github.com/motdotla/dotenv
  // https://github.com/motdotla/dotenv-expand
  dotEnvFiles.forEach((dotEnvFile: string) => {
    if (fs.existsSync(dotEnvFile)) {
      dotenvExpand(
        dotenv.config({
          path: dotEnvFile,
        }),
      );
    }
  });
};

module.exports = makeEnv;
