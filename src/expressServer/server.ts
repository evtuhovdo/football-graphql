import 'reflect-metadata';
import { createConnection } from 'typeorm';
import createExpressServer from './createExpressServer';

const startApp = async () => {
  const port: string = process.env.PORT || '80';

  const app = await createExpressServer();

  app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
  });
};

const doCreateConnection = () => {
  createConnection()
    .then(() => startApp())
    .catch((e) => {
      console.log('error', e);
      console.log('Try reconnect after 10 sec');
      setTimeout(() => {
        doCreateConnection();
      }, 10 * 1000)
    });
};

doCreateConnection();
