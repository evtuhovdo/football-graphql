import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLID,
  GraphQLString,
} from 'graphql';

const CreateMatch1x1Input = new GraphQLInputObjectType({
  name: 'CreateMatch1x1Input',
  fields: () => ({
    leftScore: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    rightScore: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    date: {
      type: new GraphQLNonNull(GraphQLString)
    },
    sportsmanLeftId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    sportsmanRightId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    leagueId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    table1x1Id: {
      type: new GraphQLNonNull(GraphQLID),
    },
  }),
});

export default CreateMatch1x1Input;
