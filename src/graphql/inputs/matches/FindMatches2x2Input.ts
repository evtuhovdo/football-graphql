import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLID,
} from 'graphql';

const FindMatches2x2Input = new GraphQLInputObjectType({
  name: 'FindMatches2x2Input',
  fields: () => ({
    leagueId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    table2x2Id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    compositionOneId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    compositionTwoId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  }),
});

export default FindMatches2x2Input;
