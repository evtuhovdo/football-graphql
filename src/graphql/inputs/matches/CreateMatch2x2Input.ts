import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLID,
  GraphQLString,
} from 'graphql';

const CreateMatch2x2Input = new GraphQLInputObjectType({
  name: 'CreateMatch2x2Input',
  fields: () => ({
    leftScore: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    rightScore: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    date: {
      type: new GraphQLNonNull(GraphQLString)
    },
    compositionLeftId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    compositionRightId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    leagueId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    table2x2Id: {
      type: new GraphQLNonNull(GraphQLID),
    },
  }),
});

export default CreateMatch2x2Input;
