import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLID,
} from 'graphql';

const FindMatches1x1Input = new GraphQLInputObjectType({
  name: 'FindMatches1x1Input',
  fields: () => ({
    leagueId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    table1x1Id: {
      type: new GraphQLNonNull(GraphQLID),
    },
    sportsmanOneId: {
      type: new GraphQLNonNull(GraphQLID)
    },
    sportsmanTwoId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  }),
});

export default FindMatches1x1Input;
