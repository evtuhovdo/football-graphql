import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLID,
  GraphQLFloat,
} from 'graphql';

const CreateOrUpdateExerciseChallengeInput = new GraphQLInputObjectType({
  name: 'CreateOrUpdateExerciseChallengeInput',
  fields: () => ({
    resultValue: {
      type: new GraphQLNonNull(GraphQLFloat),
    },
    sportsmanId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    exerciseId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    leagueId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    tableExerciseId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  }),
});

export default CreateOrUpdateExerciseChallengeInput;
