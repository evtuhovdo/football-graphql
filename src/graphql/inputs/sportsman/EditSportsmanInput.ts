import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLID,
  GraphQLString,
  GraphQLInt,
} from 'graphql';
import moment from 'moment-timezone';
import { EntityManager } from 'typeorm';
import { number, object, ObjectSchema, string } from 'yup';
import { MAX_SMALL_INT } from '../../../DBTypeLimits';
import Identity from '../../../orm/entity/Identity';
import Sportsman from '../../../orm/entity/Sportsman';
import Team from '../../../orm/entity/Team';
import capitalize from '../../../utils/capitalize';
import SexEnumType, { ISexEnum } from '../../enums/SexEnumType';
import isPhoneContain11DigitsInString from '../../validators/isPhoneContain11DigitsInString';
import { CreateSportsmanInputYupSchema, ICreateSportsmanObjectSchema } from './CreateSportsmanInput';

export default new GraphQLInputObjectType({
  name: 'EditSportsmanInput',
  fields: () => ({
    sportsmanId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    teamId: {
      type: new GraphQLNonNull(GraphQLID),
    },
    numberInTeam: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    phone: {
      type: new GraphQLNonNull(GraphQLString),
    },
    email: {
      type: GraphQLString,
    },
    firstName: {
      type: new GraphQLNonNull(GraphQLString),
    },
    lastName: {
      type: new GraphQLNonNull(GraphQLString),
    },
    thirdName: {
      type: GraphQLString,
    },
    dateOfBirth: {
      type: new GraphQLNonNull(GraphQLString),
    },
    sex: {
      type: new GraphQLNonNull(SexEnumType),
    },
  }),
});

interface IEditSportsmanObjectSchema extends ICreateSportsmanObjectSchema {
  sportsmanId: string;
}

function lowercase(value: string | null | undefined): string {
  if (!value) {
    return '';
  }

  return value.toLowerCase();
}

export const EditSportsmanInputYupSchema: (entityManager: EntityManager) => ObjectSchema<IEditSportsmanObjectSchema | undefined> = (entityManager: EntityManager) => {
  function validDate(value: string | null | undefined): boolean {
    if (!value) {
      return false;
    }

    return moment(value, 'DD.MM.YYYY').isValid();
  }

  function capitalizeName(value: string | null | undefined): string | null | undefined {
    if (value) {
      return capitalize(value);
    }

    return value;
  }

  async function testHasUniqPhone(value: string | null | undefined): Promise<boolean> {
    const whoHasThisPhone = await entityManager.findOne(Identity, {
      where: {
        phone: value,
      },
    });

    if (!whoHasThisPhone) {
      return true;
    }

    // @ts-ignore
    return whoHasThisPhone.id === parseInt(this.options.parent.sportsmanId, 10);
  }

  async function testHasUniqEmailOrNull(value: string | null | undefined): Promise<boolean> {
    if (!value) {
      return true;
    }

    const whoHasSameEmail = await entityManager.findOne(Identity, {
      where: {
        email: value,
      },
    });

    if (!whoHasSameEmail) {
      return true;
    }

    // @ts-ignore
    return whoHasSameEmail.id === parseInt(this.options.parent.sportsmanId, 10);
  }

  async function testHasUniqNumberInTeam(value: number | null | undefined): Promise<boolean> {
    // @ts-ignore
    const teamId = await this.options.parent.teamId;

    const whoHasSameNumber = await entityManager.findOne(Sportsman, {
      where: {
        numberInTeam: value,
        team: teamId,
      },
    });

    if (!whoHasSameNumber) {
      return true;
    }

    // @ts-ignore
    return whoHasSameNumber.id === parseInt(this.options.parent.sportsmanId, 10);
  }

  async function testTeamExist(value: string | null | undefined): Promise<boolean> {
    const teamExist = await entityManager.count(Team, {
      where: {
        id: value,
      },
    });

    return teamExist === 1;
  }

  async function testSportsmanExist(value: string | null | undefined): Promise<boolean> {
    const teamExist = await entityManager.count(Sportsman, {
      where: {
        id: value,
      },
    });

    return teamExist === 1;
  }

  return object({
    sportsmanId: string()
      .required('Укажите спортсмена.')
      .test(
        'sportsman exist',
        'Такого спортсмена не существует.',
        testSportsmanExist,
      ),
    teamId: string()
      .required('Номер комманды обязвтелен.')
      .test(
        'team exist',
        'Такой комманды не существует.',
        testTeamExist,
      ),
    numberInTeam: number()
      .required('Номер игрока в комманде обязателен.')
      .integer('Номер игрока в комманде должен быть целым числом.')
      .positive('Номер игрока в комманде должен быть целым числом больше нуля.')
      .min(1, 'Номер комманды должен быть целым числом больше нуля.')
      .max(MAX_SMALL_INT, `Номер комманды должен быть целым числом меньше ${MAX_SMALL_INT}.`)
      .test(
        'no same numberInTeam in team DB',
        'Такой номер есть у другого игрока в комманде.',
        testHasUniqNumberInTeam,
      ),
    phone: string()
      .required('Телефон обязателен.')
      .trim()
      .length(11, 'Телефон должен быть длинной 11 символов.')
      .test(
        'phone only digits',
        'Телефон может содержить только цифры.',
        isPhoneContain11DigitsInString,
      )
      .test(
        'no same phone in DB',
        'Телефон уже указан другому пользователю.',
        testHasUniqPhone,
      ),
    email: string()
      .trim()
      .transform(lowercase)
      .default('')
      .email('Указан не правильный e-mail.')
      .max(50, 'Максимальная длинна 50')
      .test(
        'no same email in DB',
        'E-mail уже указан другому пользователю.',
        testHasUniqEmailOrNull,
      ),
    firstName: string()
      .trim()
      .required('Имя обязательно.')
      .max(100, 'Максимальная длинна 100')
      .transform(capitalizeName),
    lastName: string()
      .trim()
      .required('Фамилия обязательна.')
      .max(100, 'Максимальная длинна 100')
      .transform(capitalizeName),
    thirdName: string()
      .trim()
      .max(100, 'Максимальная длинна 100')
      .transform(capitalizeName),
    sex: string()
      .required('Поле обязательно для заполнения.')
      .default(ISexEnum.MALE)
      .oneOf([ISexEnum.MALE, ISexEnum.FEMALE]),
    dateOfBirth: string()
      .trim()
      .required('Дата рождения обязательна.')
      .matches(/(\d\d[\.]\d\d[\.]\d\d\d\d)/, { excludeEmptyString: true, message: 'Неверный формат даты' })
      .test(
        'valid date',
        'Дата имеет не правильный формат.',
        validDate,
      ),
  });
};
