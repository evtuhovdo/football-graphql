import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLFloat,
} from 'graphql';
import SportsmanPersonalParameterNameEnumType from '../../enums/SportsmanPersonalParameterNameEnumType';

export default new GraphQLInputObjectType({
  name: 'PersonalParameterInput',
  fields: () => ({
    paramName: { type: new GraphQLNonNull(SportsmanPersonalParameterNameEnumType) },
    value: { type: new GraphQLNonNull(GraphQLFloat) },
  }),
});
