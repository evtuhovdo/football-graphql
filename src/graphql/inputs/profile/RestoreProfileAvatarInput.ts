import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLID,
} from 'graphql';

export default new GraphQLInputObjectType({
  name: 'RestoreProfileAvatarInput',
  fields: () => ({
    id: { type: new GraphQLNonNull(GraphQLID) },
  }),
});
