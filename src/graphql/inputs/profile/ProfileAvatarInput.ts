import {
  GraphQLInputObjectType,
} from 'graphql';
import { GraphQLUpload } from 'apollo-server-express';


export default new GraphQLInputObjectType({
  name: 'ProfileAvatarInput',
  // @ts-ignore
  fields: () => ({
    file: { type:
      GraphQLUpload,
    },
  }),
});
