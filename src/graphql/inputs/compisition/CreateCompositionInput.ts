import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLList,
  GraphQLString,
} from 'graphql';

const CreateCompositionInput = new GraphQLInputObjectType({
  name: 'CreateCompositionInput',
  fields: () => ({
    sportsmans: {
      type: new GraphQLList(new GraphQLNonNull(GraphQLString)),
    },
    teamId: {
      type: new GraphQLNonNull(GraphQLString),
    },
  }),
});

export default CreateCompositionInput;
