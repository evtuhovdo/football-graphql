import {
  GraphQLInputObjectType,
  GraphQLNonNull,
  GraphQLID,
} from 'graphql';

export default new GraphQLInputObjectType({
  name: 'RemoveCompositionInput',
  fields: () => ({
    id: { type: new GraphQLNonNull(GraphQLID) },
  }),
});
