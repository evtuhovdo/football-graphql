import CreateMatch1x1Mutation from './CreateMatch1x1Mutation';
import CreateMatch2x2Mutation from './CreateMatch2x2Mutation';
import CreateOrUpdateExerciseChallengeMutation from './CreateOrUpdateExerciseChallengeMutation';

export default {
  CreateMatch1x1Mutation,
  CreateMatch2x2Mutation,
  CreateOrUpdateExerciseChallengeMutation,
};
