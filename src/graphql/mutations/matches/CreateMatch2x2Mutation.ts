import { ForbiddenError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import moment from 'moment-timezone';
import Match2x2 from '../../../orm/entity/Challenge/Match2x2';
import Composition from '../../../orm/entity/Composition/Composition';
import League from '../../../orm/entity/League';

import Table2x2 from '../../../orm/entity/Table/Table2x2';
import { IResolverContext } from '../../createApolloServer';
import CreateMatch2x2Input from '../../inputs/matches/CreateMatch2x2Input';
import MutationPayloadWithErrorType from '../../types/generated/MutationPayloadWithErrorType';
import Match2x2Type from '../../types/challenge/Match2x2Type';


interface ICreateMatch2x2MutationArgs {
  input: {
    date: string;
    rightScore: number;
    leftScore: number;
    compositionRightId: string;
    compositionLeftId: string;
    leagueId: string;
    table2x2Id: string;
  }
}

const CreateMatch2x2Mutation: GraphQLFieldConfig<null, IResolverContext, ICreateMatch2x2MutationArgs> = {
  type:  MutationPayloadWithErrorType('CreateMatch2x2Mutation', Match2x2Type),
  args: {
    input: {
      type: CreateMatch2x2Input,
    },
  },
  resolve: async (source, { input }, { entityManager, identity }) => {
    if (!identity) {
      throw new ForbiddenError('Access denied to create composition. User in not login!');
    }

    const newMatch = new Match2x2();

    const mDate =moment(input.date, 'YYYY-MM-DD');
    if (!mDate.isValid()) {
      return  {
        errors: {
          date: 'Дата имеет не правильный формат',
        }
      }
    }

    if (input.leftScore < 0) {
      return  {
        errors: {
          leftScore: 'Счет не может быть отрицательным',
        }
      }
    }

    if (input.rightScore < 0) {
      return  {
        errors: {
          leftScore: 'Счет не может быть отрицательным',
        }
      }
    }

    newMatch.date = moment(input.date, 'YYYY-MM-DD').toDate();
    newMatch.leftScore = input.leftScore;
    newMatch.rightScore = input.rightScore;

    const compR = await entityManager.findOne(Composition, input.compositionRightId);
    const compL = await entityManager.findOne(Composition, input.compositionLeftId);
    const table2x2 = await entityManager.findOne(Table2x2, input.table2x2Id);
    const league = await entityManager.findOne(League, input.leagueId);

    if (!compR || !compL || !table2x2 || !league) {
      return  {
        errors: 'Неизвестная ошибка. Что-то не нашли.',
      }
    }

    newMatch.compositionLeft = Promise.resolve(compR);
    newMatch.compositionRight = Promise.resolve(compL);
    newMatch.table = Promise.resolve(table2x2);
    newMatch.league = Promise.resolve(league);

    await entityManager.save(newMatch);

    return {
      payload: newMatch
    };
  },
};

export default CreateMatch2x2Mutation;
