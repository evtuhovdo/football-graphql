import { ForbiddenError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import ExerciseChallenge from '../../../orm/entity/Challenge/ExerciseChallenge';
import Exercise from '../../../orm/entity/Exercise/Exercise';
import League from '../../../orm/entity/League';

import Sportsman from '../../../orm/entity/Sportsman';
import TableExercise from '../../../orm/entity/Table/TableExercise';
import { IResolverContext } from '../../createApolloServer';
import CreateOrUpdateExerciseChallengeInput from '../../inputs/matches/CreateOrUpdateExerciseChallengeInput';
import ExerciseChallengeType from '../../types/challenge/ExerciseChallengeType';
import MutationPayloadWithErrorType from '../../types/generated/MutationPayloadWithErrorType';


interface ICreateOrUpdateExerciseChallengeMutationArgs {
  input: {
    resultValue: number;
    sportsmanId: string;
    leagueId: string;
    exerciseId: string;
    tableExerciseId: string;
  }
}

const CreateOrUpdateExerciseChallengeMutation: GraphQLFieldConfig<null, IResolverContext, ICreateOrUpdateExerciseChallengeMutationArgs> = {
  type:  MutationPayloadWithErrorType('CreateOrUpdateExerciseChallengeMutation', ExerciseChallengeType),
  args: {
    input: {
      type: CreateOrUpdateExerciseChallengeInput,
    },
  },
  resolve: async (source, { input }, { entityManager, identity }) => {
    if (!identity) {
      throw new ForbiddenError('Access denied to create composition. User in not login!');
    }


    if (input.resultValue < 0) {
      return  {
        errors: {
          leftScore: 'Результат не может быть отрицательным',
        }
      }
    }

    const sportsman = await entityManager.findOne(Sportsman, input.sportsmanId);
    const tableExercise = await entityManager.findOne(TableExercise, input.tableExerciseId);
    const league = await entityManager.findOne(League, input.leagueId);
    const exercise = await entityManager.findOne(Exercise, input.exerciseId);

    if (!sportsman || !tableExercise || !league || !exercise) {
      return  {
        errors: 'Неизвестная ошибка. Что-то не нашли.',
      }
    }

    const match = await entityManager.findOne(ExerciseChallenge, {
      where: {
        sportsman,
        table: tableExercise,
        league,
        exercise
      }
    });

    let newMatch = new ExerciseChallenge();

    if (match) {
      newMatch = match;
    }

    newMatch.date = new Date();
    newMatch.resultValue = input.resultValue;

    newMatch.sportsman = Promise.resolve(sportsman);
    newMatch.table = Promise.resolve(tableExercise);
    newMatch.league = Promise.resolve(league);
    newMatch.exercise = Promise.resolve(exercise);

    await entityManager.save(newMatch);

    // Если все прошло хорошо, то для всех остальных упражнений в лиге надо скинуть savedInRating на false
    await entityManager.query(`
          UPDATE challenge_exercise
            SET saved_in_rating = false
          WHERE
            challenge_exercise.league_id = $1
            `,
      [league.id],
    );

    return {
      payload: newMatch
    };
  },
};

export default CreateOrUpdateExerciseChallengeMutation;
