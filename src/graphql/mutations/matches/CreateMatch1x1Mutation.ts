import { ForbiddenError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import moment from 'moment-timezone';
import Match1x1 from '../../../orm/entity/Challenge/Match1x1';
import League from '../../../orm/entity/League';

import Sportsman from '../../../orm/entity/Sportsman';
import Table1x1 from '../../../orm/entity/Table/Table1x1';
import { IResolverContext } from '../../createApolloServer';
import CreateMatch1x1Input from '../../inputs/matches/CreateMatch1x1Input';
import MutationPayloadWithErrorType from '../../types/generated/MutationPayloadWithErrorType';
import Match1x1Type from '../../types/challenge/Match1x1Type';
import SportsmanType from '../../types/SportsmanType';


interface ICreateMatch1x1MutationArgs {
  input: {
    date: string;
    rightScore: number;
    leftScore: number;
    sportsmanRightId: string;
    sportsmanLeftId: string;
    leagueId: string;
    table1x1Id: string;
  }
}

const CreateMatch1x1Mutation: GraphQLFieldConfig<null, IResolverContext, ICreateMatch1x1MutationArgs> = {
  type:  MutationPayloadWithErrorType('CreateMatch1x1Mutation', Match1x1Type),
  args: {
    input: {
      type: CreateMatch1x1Input,
    },
  },
  resolve: async (source, { input }, { entityManager, identity }) => {
    if (!identity) {
      throw new ForbiddenError('Access denied to create composition. User in not login!');
    }

    const newMatch = new Match1x1();

    const mDate =moment(input.date, 'YYYY-MM-DD');
    if (!mDate.isValid()) {
      return  {
        errors: {
          date: 'Дата имеет не правильный формат',
        }
      }
    }

    if (input.leftScore < 0) {
      return  {
        errors: {
          leftScore: 'Счет не может быть отрицательным',
        }
      }
    }

    if (input.rightScore < 0) {
      return  {
        errors: {
          leftScore: 'Счет не может быть отрицательным',
        }
      }
    }

    newMatch.date = moment(input.date, 'YYYY-MM-DD').toDate();
    newMatch.leftScore = input.leftScore;
    newMatch.rightScore = input.rightScore;

    const spR = await entityManager.findOne(Sportsman, input.sportsmanRightId);
    const spL = await entityManager.findOne(Sportsman, input.sportsmanLeftId);
    const table1x1 = await entityManager.findOne(Table1x1, input.table1x1Id);
    const league = await entityManager.findOne(League, input.leagueId);

    if (!spR || !spL || !table1x1 || !league) {
      return  {
        errors: 'Неизвестная ошибка. Что-то не нашли.',
      }
    }

    newMatch.sportsmanRight = Promise.resolve(spR);
    newMatch.sportsmanLeft = Promise.resolve(spL);
    newMatch.table = Promise.resolve(table1x1);
    newMatch.league = Promise.resolve(league);

    await entityManager.save(newMatch);

    return {
      payload: newMatch
    };
  },
};

export default CreateMatch1x1Mutation;
