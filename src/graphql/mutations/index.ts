import profile from './profile';
import sportsman from './sportsman';
import composition from './composition';
import matches from './matches';

export default {
  updateProfileAvatar: profile.UploadProfileAvatarMutation,
  removeProfileAvatar: profile.RemoveProfileAvatarMutation,
  restoreProfileAvatar: profile.RestoreProfileAvatarMutation,
  addSportsmanPersonalValue: sportsman.AddPersonalValueMutation,
  createSportsman: sportsman.CreateSportsmanMutation,
  editSportsman: sportsman.EditSportsmanMutation,
  createComposition: composition.CreateCompositionMutation,
  removeComposition: composition.RemoveCompositionMutation,
  createMatch1x1: matches.CreateMatch1x1Mutation,
  createMatch2x2: matches.CreateMatch2x2Mutation,
  createOrUpdateExerciseChallenge: matches.CreateOrUpdateExerciseChallengeMutation,
};
