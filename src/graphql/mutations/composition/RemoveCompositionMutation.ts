import { ForbiddenError } from 'apollo-server-express';
import { GraphQLBoolean, GraphQLFieldConfig } from 'graphql';
import Composition from '../../../orm/entity/Composition/Composition';
import { IResolverContext } from '../../createApolloServer';
import RemoveCompositionInput from '../../inputs/compisition/RemoveCompositionInput';


interface IRemoveCompositionMutationArgs {
  input: {
    id: string;
  }
}

const RemoveCompositionMutation: GraphQLFieldConfig<null, IResolverContext, IRemoveCompositionMutationArgs> = {
  type: GraphQLBoolean,
  args: {
    input: {
      type: RemoveCompositionInput,
    },
  },
  resolve: async (source, args, { entityManager, identity }) => {
    if (!identity) {
      throw new ForbiddenError('Access denied to remove composition. User in not login!');
    }

    const composition = await entityManager.find(Composition, {
        where: {
          id: parseInt(args.input.id, 10),
        },
      },
    );

    if (!composition) {
      return false;
    }

    const res = await entityManager.remove(composition);

    // TODO: проверить результат
    console.log('res', res);

    return true;
  },
};

export default RemoveCompositionMutation;
