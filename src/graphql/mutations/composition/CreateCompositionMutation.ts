import { ForbiddenError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';

import Composition, { CompositionEnum } from '../../../orm/entity/Composition/Composition';
import Sportsman from '../../../orm/entity/Sportsman';
import Team from '../../../orm/entity/Team';
import getCompositionCapacityByType from '../../../utils/getCompositionCapacityByType';
import { IResolverContext } from '../../createApolloServer';
import CreateCompositionInput from '../../inputs/compisition/CreateCompositionInput';
import CompositionType from '../../types/CompositionType';

interface ICreateCompositionMutationArgs {
  input: {
    sportsmans: [string];
    teamId: string;
  }
}

const CreateCompositionMutation: GraphQLFieldConfig<null, IResolverContext, ICreateCompositionMutationArgs> = {
  type: CompositionType,
  args: {
    input: {
      type: CreateCompositionInput,
    },
  },
  resolve: async (source, args, { entityManager, identity }) => {
    if (!identity) {
      throw new ForbiddenError('Access denied to create composition. User in not login!');
    }

    const newComposition = new Composition();
    newComposition.type = CompositionEnum.COMPOSITION_2x2;
    newComposition.name = null;

    const { max, min } = getCompositionCapacityByType(CompositionEnum.COMPOSITION_2x2);
    newComposition.minCapacity = min;
    newComposition.maxCapacity = max;

    const team = await entityManager.findOne(Team, args.input.teamId);

    if (!team) {
      throw new Error('Неизвестная ошибка, комманда не найдена.');
    }

    newComposition.team = Promise.resolve(team);

    const conditions = args.input.sportsmans.map((id) => {
      return {
        id: parseInt(id, 10),
        team,
      };
    });

    const sportsmans = await entityManager.find(Sportsman, {
        where: conditions,
      },
    );

    if (sportsmans.length !== args.input.sportsmans.length) {
      throw new Error('Неизвестная ошибка, количество найденных и переданных спортсменов отличается.');
    }

    newComposition.sportsmans = Promise.resolve(sportsmans);

    await entityManager.save(newComposition);

    return newComposition;
  },
};

export default CreateCompositionMutation;
