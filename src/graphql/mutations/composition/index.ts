import CreateCompositionMutation from './CreateCompositionMutation';
import RemoveCompositionMutation from './RemoveCompositionMutation';

export default {
  CreateCompositionMutation,
  RemoveCompositionMutation,
};
