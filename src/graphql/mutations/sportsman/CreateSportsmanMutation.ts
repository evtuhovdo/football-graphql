import { AuthenticationError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import moment from 'moment-timezone';

import { ValidationError } from 'yup';
import { IMutationReturnWithErrors } from '../../../models';
import Role from '../../../orm/entity/Role';
import Sportsman from '../../../orm/entity/Sportsman';
import Team from '../../../orm/entity/Team';
import { SPORTSMAN_ROLE_NAME } from '../../../rbac/constans';
import sendSms from '../../../sms/sendSms';
import getPasswordHash from '../../../utils/getPasswordHash';
import generatePassword from '../../../utils/passwordGenerator';
import schemaErrorsToMutationFieldErrors from '../../../utils/schemaErrorsToMutationFieldErrors';
import { IResolverContext } from '../../createApolloServer';
import { ISexEnum } from '../../enums/SexEnumType';
import CreateSportsmanInput, { CreateSportsmanInputYupSchema } from '../../inputs/sportsman/CreateSportsmanInput';
import { IMutationFieldError } from '../../types/common/MutationFieldError/MutationFieldErrorType';
import MutationPayloadWithErrorType from '../../types/generated/MutationPayloadWithErrorType';
import SportsmanType from '../../types/SportsmanType';

interface ICreateSportsmanMutationArgs {
  input: {
    teamId: string;
    numberInTeam: number,
    phone: string,
    email: string,
    firstName: string,
    lastName: string,
    thirdName?: string,
    dateOfBirth: string,
    sex: ISexEnum,
  }
}

const CreateSportsmanMutation: GraphQLFieldConfig<null, IResolverContext, ICreateSportsmanMutationArgs> = {
  type: MutationPayloadWithErrorType('CreateSportsmanMutation', SportsmanType),
  args: {
    input: {
      type: CreateSportsmanInput,
    },
  },
  resolve: async (root, { input }, { entityManager, identity }): Promise<IMutationReturnWithErrors<Sportsman>> => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    const yupSchema = CreateSportsmanInputYupSchema(entityManager);

    let validationErrors: IMutationFieldError[] = [];
    const filteredInput = await yupSchema.validate(input, { abortEarly: false })
      .catch((errors: ValidationError) => {
        validationErrors = schemaErrorsToMutationFieldErrors(errors);
      });

    if (validationErrors.length) {
      return {
        errors: validationErrors,
      }
    }

    if (!filteredInput) {
      return {
        errors: {
          message: 'Неизвестная ошибка.',
        },
      }
    }

    const team = await entityManager.findOne(Team, filteredInput.teamId);

    if (!team) {
      return {
        errors: {
          message: 'Неизвестная ошибка. Команда не найдена.',
        },
      }
    }

    const sportsman = new Sportsman();
    sportsman.team = Promise.resolve(team);
    sportsman.numberInTeam = filteredInput.numberInTeam;
    sportsman.phone = filteredInput.phone;
    if (filteredInput.email) {
      sportsman.email = filteredInput.email;
    }
    sportsman.firstName = filteredInput.firstName;
    sportsman.lastName = filteredInput.lastName;
    if (filteredInput.thirdName) {
      sportsman.thirdName = filteredInput.thirdName;
    }

    sportsman.sex = filteredInput.sex;

    sportsman.dateOfBirth = moment(filteredInput.dateOfBirth, 'DD.MM.YYYY').toDate();

    const password = generatePassword();
    sportsman.password = getPasswordHash(password);

    const sportsmanRole = await entityManager.findOne(Role, {
      where: {
        name: SPORTSMAN_ROLE_NAME,
      }
    });

    if (!sportsmanRole) {
      return {
        errors: {
          message: 'Неизвестная ошибка. Роль спортсмена не найдена.',
        },
      }
    }

    sportsman.role = sportsmanRole;

    const insertedSportsman = await entityManager.save(sportsman);

    const message = `Ваш доступ в новую атмосфера футбола
приложение https://shorturl.at/ikFX8
логин ${filteredInput.phone}
пароль ${password}`;

    const smsSendResult = await sendSms(message, filteredInput.phone);

    if (!smsSendResult) {
      return {
        payload: insertedSportsman,
        errors: {
          message: 'Спортсмен был создан, но смс не было отправлено.',
        },
      };
    }

    return {
      payload: insertedSportsman,
    };
  },
};

export default CreateSportsmanMutation;
