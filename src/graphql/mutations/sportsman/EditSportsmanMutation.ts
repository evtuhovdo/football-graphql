import { AuthenticationError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import moment from 'moment-timezone';

import { ValidationError } from 'yup';
import { IMutationReturnWithErrors } from '../../../models';
import Sportsman from '../../../orm/entity/Sportsman';
import Team from '../../../orm/entity/Team';
import schemaErrorsToMutationFieldErrors from '../../../utils/schemaErrorsToMutationFieldErrors';
import { IResolverContext } from '../../createApolloServer';
import { ISexEnum } from '../../enums/SexEnumType';
import EditSportsmanInput, { EditSportsmanInputYupSchema } from '../../inputs/sportsman/EditSportsmanInput';
import { IMutationFieldError } from '../../types/common/MutationFieldError/MutationFieldErrorType';
import MutationPayloadWithErrorType from '../../types/generated/MutationPayloadWithErrorType';
import SportsmanType from '../../types/SportsmanType';

interface IEditSportsmanMutationArgs {
  input: {
    sportsmanId: string;
    teamId: string;
    numberInTeam: number,
    phone: string,
    email: string,
    firstName: string,
    lastName: string,
    thirdName?: string,
    dateOfBirth: string,
    sex: ISexEnum,
  }
}

const EditSportsmanMutation: GraphQLFieldConfig<null, IResolverContext, IEditSportsmanMutationArgs> = {
  type: MutationPayloadWithErrorType('EditSportsmanMutation', SportsmanType),
  args: {
    input: {
      type: EditSportsmanInput,
    },
  },
  resolve: async (root, { input }, { entityManager, identity }): Promise<IMutationReturnWithErrors<Sportsman>> => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    const yupSchema = EditSportsmanInputYupSchema(entityManager);

    let validationErrors: IMutationFieldError[] = [];
    const filteredInput = await yupSchema.validate(input, { abortEarly: false })
      .catch((errors: ValidationError) => {
        validationErrors = schemaErrorsToMutationFieldErrors(errors);
      });

    if (validationErrors.length) {
      return {
        errors: validationErrors,
      }
    }

    if (!filteredInput) {
      return {
        errors: {
          message: 'Неизвестная ошибка.',
        },
      }
    }

    const team = await entityManager.findOne(Team, filteredInput.teamId);

    if (!team) {
      return {
        errors: {
          message: 'Неизвестная ошибка. Команда не найдена.',
        },
      }
    }

    const sportsman = await entityManager.findOne(Sportsman, filteredInput.sportsmanId);

    if (!sportsman) {
      return {
        errors: {
          message: 'Неизвестная ошибка. Спортсмен не найден.',
        },
      }
    }

    sportsman.team = Promise.resolve(team);
    sportsman.numberInTeam = filteredInput.numberInTeam;
    sportsman.phone = filteredInput.phone;
    if (filteredInput.email) {
      sportsman.email = filteredInput.email;
    }
    sportsman.firstName = filteredInput.firstName;
    sportsman.lastName = filteredInput.lastName;
    if (filteredInput.thirdName) {
      sportsman.thirdName = filteredInput.thirdName;
    }

    sportsman.sex = filteredInput.sex;

    sportsman.dateOfBirth = moment(filteredInput.dateOfBirth, 'DD.MM.YYYY').toDate();

    const savesSportsman = await entityManager.save(sportsman);

    return {
      payload: savesSportsman,
    };
  },
};

export default EditSportsmanMutation;
