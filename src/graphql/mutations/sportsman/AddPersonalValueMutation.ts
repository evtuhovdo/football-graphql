import { AuthenticationError, ApolloError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import Sportsman from '../../../orm/entity/Sportsman';
import SportsmansPersonalParams, { SportsmansPersonalParamNameEnum } from '../../../orm/entity/SportsmansPersonalParams';
import { IResolverContext } from '../../createApolloServer';
import PersonalParameterInput from '../../inputs/sportsman/PersonalParameterInput';
import SportsmanParamType from '../../types/SportsmanParamType';

interface IAddPersonalValueMutationArgs {
  input: {
    paramName: SportsmansPersonalParamNameEnum
    value: number;
  }
}

const AddPersonalValueMutation: GraphQLFieldConfig<null, IResolverContext, IAddPersonalValueMutationArgs> = {
  type: SportsmanParamType,
  args: {
    input: {
      type: PersonalParameterInput,
    },
  },
  resolve: async (root, args, { entityManager, identity }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    const sportsman = await entityManager.findOne(Sportsman, identity.id);

    if (!sportsman) {
      throw new ApolloError('You should be sportsman');
    }

    const { paramName, value } = args.input;

    const newParam = new SportsmansPersonalParams();
    newParam.name = paramName;
    newParam.value = value;
    newParam.sportsman = sportsman;

    return entityManager.save(newParam);
  },
};

export default AddPersonalValueMutation;
