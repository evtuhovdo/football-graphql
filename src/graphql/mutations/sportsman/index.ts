import AddPersonalValueMutation from './AddPersonalValueMutation';
import CreateSportsmanMutation from './CreateSportsmanMutation';
import EditSportsmanMutation from './EditSportsmanMutation';

export default {
  AddPersonalValueMutation,
  CreateSportsmanMutation,
  EditSportsmanMutation,
};
