import { ForbiddenError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import IdentityOldAvatar from '../../../orm/entity/IdentityOldAvatar';
import { IResolverContext } from '../../createApolloServer';
import ProfileType from '../../types/ProfileType';


const RemoveProfileAvatarMutation: GraphQLFieldConfig<null, IResolverContext, null> = {
  type: ProfileType,
  resolve: async (source, args, { entityManager, identity }) => {
    if (!identity) {
      throw new ForbiddenError('Access denied');
    }

    if (!identity.avatarURL) {
      return identity;
    }

    if (identity.avatarURL) {
      const identityOldAvatar = new IdentityOldAvatar();
      identityOldAvatar.user = identity;
      identityOldAvatar.url = identity.avatarURL;

      await entityManager.save(identityOldAvatar);
    }

    identity.avatarURL = null;

    return await entityManager.save(identity);
  },
};

export default RemoveProfileAvatarMutation;
