import { ForbiddenError, UserInputError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import Identity from '../../../orm/entity/Identity';
import IdentityOldAvatar from '../../../orm/entity/IdentityOldAvatar';
import { IResolverContext } from '../../createApolloServer';
import RestoreProfileAvatarInput from '../../inputs/profile/RestoreProfileAvatarInput';
import ProfileType from '../../types/ProfileType';


interface IRestoreProfileAvatarMutationArgs {
  input: {
    id: string;
  }
}

const RestoreProfileAvatarMutation: GraphQLFieldConfig<null, IResolverContext, IRestoreProfileAvatarMutationArgs> = {
  type: ProfileType,
  args: {
    input: {
      type: RestoreProfileAvatarInput,
    },
  },
  resolve: async (source, args, { entityManager, identity }) => {
    if (!identity) {
      throw new ForbiddenError('Access denied');
    }

    const oldAvatar = await entityManager.findOne(
      IdentityOldAvatar,
      {
        where: {
          user: identity.id,
          id: args.input.id,
        },
      },
    );

    if (!oldAvatar) {
      throw new UserInputError('Can`t find old avatar and restore it.');
    }

    if (identity.avatarURL) {
      const identityOldAvatar = new IdentityOldAvatar();
      identityOldAvatar.user = identity;
      identityOldAvatar.url = identity.avatarURL;

      await entityManager.save(identityOldAvatar);
    }

    identity.avatarURL = oldAvatar.url;

    return await entityManager.save(identity);
  },
};

export default RestoreProfileAvatarMutation;
