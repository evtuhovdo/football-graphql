import UploadProfileAvatarMutation from './UploadProfileAvatarMutation';
import RemoveProfileAvatarMutation from './RemoveProfileAvatarMutation';
import RestoreProfileAvatarMutation from './RestoreProfileAvatarMutation';

export default {
  UploadProfileAvatarMutation,
  RemoveProfileAvatarMutation,
  RestoreProfileAvatarMutation,
};
