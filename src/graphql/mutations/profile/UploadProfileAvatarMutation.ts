import { ApolloError } from 'apollo-server-errors';
import { ForbiddenError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import { FileUpload } from 'graphql-upload';
import { v4 as uuid } from 'uuid';
import fs from 'fs';
import { createWriteStream, unlink } from 'fs';
import IdentityOldAvatar from '../../../orm/entity/IdentityOldAvatar';
import { SportsmansPersonalParamNameEnum } from '../../../orm/entity/SportsmansPersonalParams';
import putObject from '../../../s3/putObject';
import { IResolverContext } from '../../createApolloServer';
import ProfileAvatarInput from '../../inputs/profile/ProfileAvatarInput';
import ProfileType from '../../types/ProfileType';

interface IUploadProfileAvatarMutationArgs {
  input: {
    file: FileUpload,
    paramName: SportsmansPersonalParamNameEnum
    value: number;
  }
}

const UploadProfileAvatarMutation: GraphQLFieldConfig<null, IResolverContext, IUploadProfileAvatarMutationArgs> = {
  type: ProfileType,
  args: {
    input: {
      type: ProfileAvatarInput,
    },
  },
  resolve: async (source, args, { entityManager, identity }) => {
    if (!identity) {
      throw new ForbiddenError('Access denied to change avatar. User in not login!');
    }

    const { file } = args.input;
    const { createReadStream, filename, mimetype } = await file;

    const appDirectory = fs.realpathSync(process.cwd());
    const UPLOAD_DIR = `${appDirectory}/tmp`;
    const id = uuid();
    const stream = createReadStream();
    const newFilename = `${id}-${filename}`;
    const path = `${UPLOAD_DIR}/${newFilename}`;

    // Делаем локальную копию файла.
    await new Promise((resolve, reject) => {
      stream
        .on('error', (error) => {
          console.error(error);
          // Удаляем временный файл
          unlink(path, () => {
            reject(error);
          });
        })
        .pipe(createWriteStream(path))
        .on('error', reject)
        .on('finish', () => {
          console.log(`upload tmp file ${path} success`);

          return resolve();
        });
    });

    const userId = identity.id;
    const body = createReadStream();

    const bucket = process.env.BUCKET_MAIN;

    if (!bucket) {
      throw new ApolloError('BUCKET_MAIN not specified in config');
    }
    const fileKey = `avatars/avatar-${userId}-${newFilename}`;

    const data = await putObject({
      body,
      bucket,
      fileKey,
      ACL: 'public-read',
      ContentType: mimetype,
    });

    const publicFilePrefix = process.env.PUBLIC_FILE_PREFIX || '';

    const avatarURL = `${publicFilePrefix}/${fileKey}`;

    if (!data.ETag) {
      console.error('Ошибка при загрузке файла.');

      return identity;
    }

    // Удаляем временный файл
    unlink(path, () => {
      console.log(`Файл ${path} - удален`);
    });

    if (identity.avatarURL) {
      const identityOldAvatar = new IdentityOldAvatar();
      identityOldAvatar.user = identity;
      identityOldAvatar.url = identity.avatarURL;

      await entityManager.save(identityOldAvatar);
    }

    identity.avatarURL = avatarURL;

    return await entityManager.save(identity);
  },
};

export default UploadProfileAvatarMutation;
