import { GraphQLEnumType } from 'graphql';

export enum ISportsmanPersonalParameterNameEnum {
  FAT_PERCENT = 'FAT_PERCENT',
  HEIGHT = 'HEIGHT',
  WEIGHT = 'WEIGHT',
  BICEPS = 'BICEPS',
  CHEST = 'CHEST',
  WAIST = 'WAIST',
  HIP = 'HIP',
  SHIN = 'SHIN',
}

const SportsmanPersonalParameterNameEnumType = new GraphQLEnumType({
  name: 'SportsmanPersonalParameterNameEnumType',
  values: {
    [ISportsmanPersonalParameterNameEnum.FAT_PERCENT]: {
      value: ISportsmanPersonalParameterNameEnum.FAT_PERCENT,
    },
    [ISportsmanPersonalParameterNameEnum.HEIGHT]: {
      value: ISportsmanPersonalParameterNameEnum.HEIGHT,
    },
    [ISportsmanPersonalParameterNameEnum.WEIGHT]: {
      value: ISportsmanPersonalParameterNameEnum.WEIGHT,
    },
    [ISportsmanPersonalParameterNameEnum.BICEPS]: {
      value: ISportsmanPersonalParameterNameEnum.BICEPS,
    },
    [ISportsmanPersonalParameterNameEnum.CHEST]: {
      value: ISportsmanPersonalParameterNameEnum.CHEST,
    },
    [ISportsmanPersonalParameterNameEnum.WAIST]: {
      value: ISportsmanPersonalParameterNameEnum.WAIST,
    },
    [ISportsmanPersonalParameterNameEnum.HIP]: {
      value: ISportsmanPersonalParameterNameEnum.HIP,
    },
    [ISportsmanPersonalParameterNameEnum.SHIN]: {
      value: ISportsmanPersonalParameterNameEnum.SHIN,
    },
  },
});

export default SportsmanPersonalParameterNameEnumType;
