import { GraphQLEnumType } from 'graphql';
import { CompositionEnum } from '../../orm/entity/Composition/Composition';

const CompositionTypeEnumType = new GraphQLEnumType({
  name: 'CompositionTypeEnumType',
  values: {
    [CompositionEnum.COMPOSITION_2x2]: {
      value: CompositionEnum.COMPOSITION_2x2,
    },
  },
});

export default CompositionTypeEnumType;
