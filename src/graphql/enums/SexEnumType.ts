import { GraphQLEnumType } from 'graphql';

export enum ISexEnum {
  MALE = 'M',
  FEMALE = 'F',
}

const SexEnumType = new GraphQLEnumType({
  name: 'SexEnumType',
  values: {
    [ISexEnum.MALE]: {
      value: ISexEnum.MALE,
    },
    [ISexEnum.FEMALE]: {
      value: ISexEnum.FEMALE,
    },
  },
});

export default SexEnumType;
