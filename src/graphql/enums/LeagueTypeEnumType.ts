import { GraphQLEnumType } from 'graphql';

export enum ILeagueTypeEnum {
  LEAGUE_1x1 = 'LEAGUE_1x1',
  LEAGUE_2x2 = 'LEAGUE_2x2',
  EXERCISE_LEAGUE = 'EXERCISE_LEAGUE',
}

const LeagueTypeEnumType = new GraphQLEnumType({
  name: 'LeagueTypeEnumType',
  values: {
    [ILeagueTypeEnum.LEAGUE_1x1]: {
      value: ILeagueTypeEnum.LEAGUE_1x1,
    },
    [ILeagueTypeEnum.LEAGUE_2x2]: {
      value: ILeagueTypeEnum.LEAGUE_2x2,
    },
    [ILeagueTypeEnum.EXERCISE_LEAGUE]: {
      value: ILeagueTypeEnum.EXERCISE_LEAGUE,
    },
  },
});

export default LeagueTypeEnumType;
