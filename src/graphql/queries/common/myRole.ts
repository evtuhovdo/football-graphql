import {
  GraphQLString,
  GraphQLScalarType,
} from 'graphql';
import { GUEST_ROLE_NAME } from '../../../rbac/constans';
import { IResolverContext } from '../../createApolloServer';

interface IMyRole {
  resolve: (root: any, args: any, context: IResolverContext) => Promise<string>;
  type: GraphQLScalarType
}

const myRole: IMyRole = {
  type: GraphQLString,
  resolve: async (root, args, { identity }) => {
    if (!identity) {
      return GUEST_ROLE_NAME;
    }

    return identity.role.name;
  },
};

export default myRole;
