import {
  GraphQLBoolean,
  GraphQLScalarType,
} from 'graphql';
import { IResolverContext } from '../../createApolloServer';

interface IIsAuthenticated {
  type: GraphQLScalarType
  resolve: (root: any, args: any, context: IResolverContext) => Promise<boolean>;
}

const isAuthenticated: IIsAuthenticated = {
  type: GraphQLBoolean,
  resolve: async (root, args, { identity }) => !!identity,
};

export default isAuthenticated;
