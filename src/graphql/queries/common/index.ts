import isAuthenticated from './isAuthenticated';
import myRole from './myRole';

export default {
  isAuthenticated,
  myRole,
};
