import profileOldAvatars from './profileOldAvatars';
import profile from './profile';

export default {
  profile,
  profileOldAvatars,
};
