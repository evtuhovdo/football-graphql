import { AuthenticationError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import { IResolverContext } from '../../createApolloServer';
import ProfileType from '../../types/ProfileType';

const profile: GraphQLFieldConfig<null, IResolverContext, null> =  {
  type: ProfileType,
  resolve: async (root, args, { identity }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return identity;
  },
};

export default profile;
