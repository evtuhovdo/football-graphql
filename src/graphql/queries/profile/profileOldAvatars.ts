import { AuthenticationError } from 'apollo-server-express';
import {
  GraphQLFieldConfig,
  GraphQLList,
  GraphQLNonNull,
} from 'graphql';
import IdentityOldAvatar from '../../../orm/entity/IdentityOldAvatar';
import { IResolverContext } from '../../createApolloServer';
import ProfileOldAvatarType from '../../types/ProfileOldAvatarType';


const profileOldAvatars: GraphQLFieldConfig<null, IResolverContext, null> = {
  type: new GraphQLList(new GraphQLNonNull(ProfileOldAvatarType)),
  resolve: async (root, args, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return entityManager.find(
      IdentityOldAvatar,
      {
        where: {
          user: identity.id,
        },
      },
    );
  },
};

export default profileOldAvatars;
