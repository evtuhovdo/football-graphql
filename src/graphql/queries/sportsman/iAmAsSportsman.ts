import { AuthenticationError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import Sportsman from '../../../orm/entity/Sportsman';
import { IResolverContext } from '../../createApolloServer';
import SportsmanType from '../../types/SportsmanType';

const iAmAsSportsman: GraphQLFieldConfig<null, IResolverContext, null> = {
  type: SportsmanType,
  resolve: async (root, args, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return entityManager.findOne(Sportsman, identity.id);
  },
};

export default iAmAsSportsman;
