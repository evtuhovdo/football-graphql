import sportsmanById from './sportsmanById';
import iAmAsSportsman from './iAmAsSportsman';

export default {
  sportsmanById,
  iAmAsSportsman,
};
