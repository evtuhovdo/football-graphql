import { AuthenticationError } from 'apollo-server-express';
import { GraphQLNonNull, GraphQLID, GraphQLFieldConfig } from 'graphql';
import Sportsman from '../../../orm/entity/Sportsman';
import { IResolverContext } from '../../createApolloServer';
import SportsmanType from '../../types/SportsmanType';

interface ISportsmanByIdArgs {
  sportsmanId: string;
}

const sportsmanById: GraphQLFieldConfig<null, IResolverContext, ISportsmanByIdArgs> = {
  type: SportsmanType,
  args: {
    sportsmanId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: async (root, { sportsmanId }, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied to sportsmanById!');
    }

    return entityManager.findOne(Sportsman, parseInt(sportsmanId, 10));
  },
};

export default sportsmanById;
