import { AuthenticationError } from 'apollo-server-express';
import { GraphQLID, GraphQLNonNull, GraphQLFieldConfig } from 'graphql';
import Table2x2 from '../../../orm/entity/Table/Table2x2';
import { IResolverContext } from '../../createApolloServer';
import Table2x2Type from '../../types/table/Table2x2Type';

interface ITable2x2ByIdArgs {
  table2x2Id: string,
}

const table2x2ById: GraphQLFieldConfig<null, IResolverContext, ITable2x2ByIdArgs> = {
  type: Table2x2Type,
  args: {
    table2x2Id: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: async (root, { table2x2Id }, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return entityManager.findOne(Table2x2, table2x2Id);
  },
};

export default table2x2ById;

