import { AuthenticationError } from 'apollo-server-express';
import { GraphQLID, GraphQLNonNull, GraphQLFieldConfig } from 'graphql';
import Table1x1 from '../../../orm/entity/Table/Table1x1';
import { IResolverContext } from '../../createApolloServer';
import Table1x1Type from '../../types/table/Table1x1Type';

interface ITable1x1ByIdArgs {
  table1x1Id: string,
}

const table1x1ById: GraphQLFieldConfig<null, IResolverContext, ITable1x1ByIdArgs> = {
  type: Table1x1Type,
  args: {
    table1x1Id: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: async (root, { table1x1Id }, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return entityManager.findOne(Table1x1, table1x1Id);
  },
};

export default table1x1ById;

