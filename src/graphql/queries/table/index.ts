import table1x1ById from './table1x1ById';
import table2x2ById from './table2x2ById';

export default {
  table1x1ById,
  table2x2ById,
};
