import { AuthenticationError } from 'apollo-server-express';
import { GraphQLList, GraphQLNonNull, GraphQLFieldConfig } from 'graphql';
import { Brackets } from 'typeorm';
import Match2x2 from '../../../orm/entity/Challenge/Match2x2';
import { IResolverContext } from '../../createApolloServer';
import FindMatches2x2Input from '../../inputs/matches/FindMatches2x2Input';
import Match2x2Type from '../../types/challenge/Match2x2Type';

interface IFindMatch2x2Args {
  input: {
    leagueId: string,
    table2x2Id: string,
    compositionOneId: string,
    compositionTwoId: string,
  }
}

const findMatches2x2: GraphQLFieldConfig<null, IResolverContext, IFindMatch2x2Args> = {
  type: new GraphQLList(new GraphQLNonNull(Match2x2Type)),
  args: {
    input: {
      type: FindMatches2x2Input,
    },
  },
  resolve: async (root, { input }, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return await entityManager
      .getRepository(Match2x2)
      .createQueryBuilder('m')
      .leftJoin('m.league', 'league')
      .leftJoin('m.table', 'table')
      .leftJoin('m.compositionLeft', 'compositionLeft')
      .leftJoin('m.compositionRight', 'compositionRight')
      .where(new Brackets(qb => {
        qb.where('compositionLeft.id = :compositionOneId AND compositionRight.id = :compositionTwoId', {
          compositionOneId: input.compositionOneId,
          compositionTwoId: input.compositionTwoId,
        })
          .orWhere('compositionLeft.id = :compositionTwoId AND compositionRight.id = :compositionOneId', {
            compositionOneId: input.compositionOneId,
            compositionTwoId: input.compositionTwoId,
          })
      }))
      .andWhere('league.id = :leagueId', { leagueId: input.leagueId })
      .andWhere('table.id = :tableId', { tableId: input.table2x2Id })
      .orderBy('m.id', 'DESC')
      .getMany();
  },
};

export default findMatches2x2;
