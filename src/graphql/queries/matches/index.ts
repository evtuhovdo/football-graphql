import findMatches1x1 from './findMatches1x1';
import findMatches2x2 from './findMatches2x2';

export default {
  findMatches1x1,
  findMatches2x2,
};
