import { AuthenticationError } from 'apollo-server-express';
import { GraphQLList, GraphQLNonNull, GraphQLFieldConfig } from 'graphql';
import { Brackets } from 'typeorm';
import Match1x1 from '../../../orm/entity/Challenge/Match1x1';
import { IResolverContext } from '../../createApolloServer';
import FindMatches1x1Input from '../../inputs/matches/FindMatches1x1Input';
import Match1x1Type from '../../types/challenge/Match1x1Type';

interface IFindMatch1x1Args {
  input: {
    leagueId: string,
    table1x1Id: string,
    sportsmanOneId: string,
    sportsmanTwoId: string,
  }
}

const findMatches1x1: GraphQLFieldConfig<null, IResolverContext, IFindMatch1x1Args> = {
  type: new GraphQLList(new GraphQLNonNull(Match1x1Type)),
  args: {
    input: {
      type: FindMatches1x1Input,
    },
  },
  resolve: async (root, { input }, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return await entityManager
      .getRepository(Match1x1)
      .createQueryBuilder('m')
      .leftJoin('m.league', 'league')
      .leftJoin('m.table', 'table')
      .leftJoin('m.sportsmanLeft', 'sportsmanLeft')
      .leftJoin('m.sportsmanRight', 'sportsmanRight')
      .where(new Brackets(qb => {
        qb.where('sportsmanLeft.id = :sportsmanOneId AND sportsmanRight.id = :sportsmanTwoId', {
          sportsmanOneId: input.sportsmanOneId,
          sportsmanTwoId: input.sportsmanTwoId,
        })
          .orWhere('sportsmanLeft.id = :sportsmanTwoId AND sportsmanRight.id = :sportsmanOneId', {
            sportsmanOneId: input.sportsmanOneId,
            sportsmanTwoId: input.sportsmanTwoId,
          })
      }))
      .andWhere('league.id = :leagueId', { leagueId: input.leagueId })
      .andWhere('table.id = :tableId', { tableId: input.table1x1Id })
      .orderBy('m.id', 'DESC')
      .getMany();
  },
};

export default findMatches1x1;
