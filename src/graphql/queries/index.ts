import common from './common';
import sportsman from './sportsman';
import profile from './profile';
import leagues from './leagues';
import video from './video';
import trainer from './trainer';
import matches from './matches';
import table from './table';
import composition from './composition';

export default {
  ...common,
  ...profile,
  ...sportsman,
  ...leagues,
  ...video,
  ...trainer,
  ...matches,
  ...table,
  ...composition,
};
