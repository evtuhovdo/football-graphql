import { AuthenticationError } from 'apollo-server-express';
import { GraphQLID, GraphQLNonNull, GraphQLFieldConfig } from 'graphql';
import Composition from '../../../orm/entity/Composition/Composition';
import { IResolverContext } from '../../createApolloServer';
import CompositionType from '../../types/CompositionType';

interface ICompositionByIdArgs {
  compositionId: string,
}

const compositionById: GraphQLFieldConfig<null, IResolverContext, ICompositionByIdArgs> = {
  type: CompositionType,
  args: {
    compositionId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: async (root, { compositionId }, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return entityManager.findOne(Composition, compositionId);
  },
};

export default compositionById;

