import { AuthenticationError } from 'apollo-server-express';
import {
  GraphQLFieldConfig,
  GraphQLList,
  GraphQLNonNull,
} from 'graphql';
import ExerciseVideo from '../../../orm/entity/Exercise/ExerciseVideo';
import { IResolverContext } from '../../createApolloServer';
import ExerciseDemoVideoType from '../../types/exercise/ExerciseDemoVideoType';

const exerciseDemoVideos: GraphQLFieldConfig<null, IResolverContext, null>  = {
  type: new GraphQLList(new GraphQLNonNull(ExerciseDemoVideoType)),
  resolve: async (root, args, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return entityManager.find(ExerciseVideo);
  },
};

export default exerciseDemoVideos;
