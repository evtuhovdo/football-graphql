import trainerById from './trainerById';
import iAmAsTrainer from './iAmAsTrainer';

export default {
  trainerById,
  iAmAsTrainer,
};
