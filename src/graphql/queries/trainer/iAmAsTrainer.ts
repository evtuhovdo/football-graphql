import { AuthenticationError } from 'apollo-server-express';
import { GraphQLFieldConfig } from 'graphql';
import Trainer from '../../../orm/entity/Trainer';
import { IResolverContext } from '../../createApolloServer';
import TrainerType from '../../types/TrainerType';

const iAmAsTrainer: GraphQLFieldConfig<null, IResolverContext, null> = {
  type: TrainerType,
  resolve: async (root, args, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied to iAmAsTrainer!');
    }

    return entityManager.findOne(Trainer, identity.id);
  },
};

export default iAmAsTrainer;
