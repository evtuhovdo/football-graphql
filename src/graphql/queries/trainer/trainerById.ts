import { AuthenticationError } from 'apollo-server-express';
import { GraphQLNonNull, GraphQLID, GraphQLFieldConfig } from 'graphql';
import Trainer from '../../../orm/entity/Trainer';
import { IResolverContext } from '../../createApolloServer';
import TrainerType from '../../types/TrainerType';

interface ITrainerByIdArgs {
  trainerId: string;
}

const trainerById: GraphQLFieldConfig<null, IResolverContext, ITrainerByIdArgs> = {
  type: TrainerType,
  args: {
    trainerId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: async (root, { trainerId }, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied to trainerById!');
    }

    return entityManager.findOne(Trainer, trainerId);
  },
};

export default trainerById;
