import { AuthenticationError } from 'apollo-server-express';
import { GraphQLID, GraphQLNonNull, GraphQLFieldConfig } from 'graphql';
import League from '../../../orm/entity/League';
import { IResolverContext } from '../../createApolloServer';
import LeagueType from '../../types/LeagueType';

interface ILeagueByIdArgs {
  leagueId: string,
}

const leagueById: GraphQLFieldConfig<null, IResolverContext, ILeagueByIdArgs> = {
  type: LeagueType,
  args: {
    leagueId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: async (root, { leagueId }, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return entityManager.findOne(League, leagueId);
  },
};

export default leagueById;

