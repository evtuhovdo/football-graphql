import { AuthenticationError } from 'apollo-server-express';
import { GraphQLFieldConfig, GraphQLNonNull, GraphQLList } from 'graphql';
import League from '../../../orm/entity/League';
import { IResolverContext } from '../../createApolloServer';
import LeagueType from '../../types/LeagueType';

const leagues: GraphQLFieldConfig<null, IResolverContext, null> = {
  type: new GraphQLNonNull(new GraphQLList(LeagueType)),
  resolve: async (root, args, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    return entityManager.find(League);
  },
};

export default leagues;

