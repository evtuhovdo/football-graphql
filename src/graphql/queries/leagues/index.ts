import leagueById from './leagueById';
import leaguesByType from './leaguesByType';
import leagues from './leagues';

export default {
  leagues,
  leagueById,
  leaguesByType,
};
