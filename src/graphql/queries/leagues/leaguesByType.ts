import { AuthenticationError } from 'apollo-server-express';
import { GraphQLNonNull, GraphQLList, GraphQLFieldConfig } from 'graphql';
import League, { LeagueTypeEnum } from '../../../orm/entity/League';
import { IResolverContext } from '../../createApolloServer';
import LeagueTypeEnumType from '../../enums/LeagueTypeEnumType';
import LeagueType from '../../types/LeagueType';

interface ILeaguesByTypeArgs {
  leagueType: LeagueTypeEnum;
}

const leaguesByType: GraphQLFieldConfig<null, IResolverContext, ILeaguesByTypeArgs> = {
  type: new GraphQLList(new GraphQLNonNull(LeagueType)),
  args: {
    leagueType: {
      type: new GraphQLNonNull(LeagueTypeEnumType),
    },
  },
  resolve: async (root, { leagueType }, { identity, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied');
    }

    const rLeague = entityManager.getRepository(League);

    return rLeague.find({ type: leagueType });
  },
};

export default leaguesByType;
