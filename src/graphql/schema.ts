import {
  GraphQLObjectType,
  GraphQLSchema,
} from 'graphql';
import { IResolverContext } from './createApolloServer';

import queries from './queries';
import mutations from './mutations';


const schemaConfig = {
  query: new GraphQLObjectType<any, IResolverContext, any>({
    name: 'Query',
    fields: queries,
  }),
  mutation: new GraphQLObjectType<any, IResolverContext, any>({
    name: 'Mutation',
    fields: mutations,
  }),
};

export default new GraphQLSchema(schemaConfig);
