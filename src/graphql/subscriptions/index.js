import common from './common';
import sportsman from './sportsman';
import profile from './profile';
import season from './season';

export default {
  ...common,
  ...profile,
  ...sportsman,
  ...season,
};
