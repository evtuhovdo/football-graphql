import { ForbiddenError, AuthenticationError } from 'apollo-server-express';
import { GraphQLNonNull, GraphQLID } from 'graphql';
import Sportsman from '../../../orm/entity/Sportsman';
import SportsmanType from '../../types/SportsmanType';

export default {
  type: SportsmanType,
  args: {
    sportsmanId: {
      type: new GraphQLNonNull(GraphQLID),
    },
  },
  resolve: async (root, { sportsmanId }, { identity, acl, entityManager }) => {
    if (!identity) {
      throw new AuthenticationError('Access denied to sportsmanById!');
    }

    if (identity && identity.id && sportsmanId === String(identity.id)) {
      const canViewOwn = await acl.can('viewOwn', 'sportsman');

      if (!canViewOwn) {
        throw new ForbiddenError('Access denied to sportsmanById eq!');
      }

      return entityManager.findOne(Sportsman, sportsmanId);
    }

    throw new ForbiddenError('Access denied to sportsmanById any!');
  },
};
