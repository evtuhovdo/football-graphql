import { ApolloServer } from 'apollo-server-express';
import { Request, Response } from 'oauth2-server';
import { EntityManager, getManager } from 'typeorm';
import { runScoreAndRatingCalculation } from '../cron-job/Cron';
import { IToken } from '../oauth/createOauthModel';

import Identity from '../orm/entity/Identity';
import schema from './schema';

interface IGetIdentity {
  ({ oauth, req, res }: { oauth: any; req: any; res: any }): Promise<Identity | null>;
}

const _getIdentity: IGetIdentity = async ({ oauth, req, res }) => {
  const request = new Request(req);
  const response = new Response(res);

  const entityManager = getManager();

  return oauth.authenticate(request, response)
    .then(
      (token: IToken) => entityManager.findOne(Identity, token.user.id),
      (error: any) => {
        console.log('error', error);
        console.error('У пользователя задан токен, но самого пользователя мы не нашли');

        return Promise.resolve(null);
      },
    );
};

export interface IResolverContext {
  entityManager: EntityManager;
  identity: Identity | null;
}

const createApolloServer: ({ oauth }: { oauth: any }) => ApolloServer = ({ oauth }) => {
  const entityManager = getManager();

  const CRON_ENABLED = process.env.CRON_ENABLED === 'yes';
  if (CRON_ENABLED) {
    let cronInProgress = false;

    setInterval(async () => {
      try {
        console.log('cronInProgress', cronInProgress);
        if (!cronInProgress) {
          cronInProgress = true;
          await runScoreAndRatingCalculation(entityManager);
          cronInProgress = false;
        }
      } catch (e) {
        console.error(e);
        console.log('CRON упал =(');
        cronInProgress = false;
      }
    }, 60 * 1000); // раз в минуту
  }

  return new ApolloServer({
    schema,
    debug: process.env.GRAPHQL_DEBUG === 'no',
    uploads: {
      maxFileSize: 10000000, // 10 MB
      maxFiles: 20,
    },
    introspection: process.env.GRAPHQL_INTROSPECTION === 'yes',
    rootValue: null,
    context: async ({ req, res }): Promise<IResolverContext> => {
      const entityManager = getManager();
      const identity = await _getIdentity({ oauth, req, res });

      return ({
        entityManager,
        identity,
      });
    },
  });
};

export default createApolloServer;
