import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID, GraphQLNonNull, GraphQLList,
} from 'graphql';
import Table1x1 from '../../orm/entity/Table/Table1x1';
import Trainer from '../../orm/entity/Trainer';
import Table1x1Type from './table/Table1x1Type';
import Table2x2Type from './table/Table2x2Type';
import TableExerciseType from './table/TableExerciseType';
import TeamType from './TeamType';
import { AuthenticationError } from 'apollo-server-express';

export default new GraphQLObjectType({
  name: 'Trainer',
  description: 'Профиль тренера',
  fields() {
    return {
      id: {
        type: GraphQLID,
      },
      firstName: {
        type: GraphQLString,
      },
      lastName: {
        type: GraphQLString,
      },
      thirdName: {
        type: GraphQLString,
      },
      fullName: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (trainer: Trainer) => [trainer.firstName, trainer.lastName, trainer.thirdName]
          .join(' ').replace('  ', ' ').trim(),
      },
      trainerTeams: {
        description: 'Комманды тренера',
        type: new GraphQLList(new GraphQLNonNull(TeamType)),
        resolve: async (trainer: Trainer, args, { identity }) => {
          if (!identity) {
            throw new AuthenticationError('Access denied to trainerTeams!');
          }

          return await trainer.teams;
        },
      },
      tables1x1: {
        description: 'Турнирные таблицы 1х1 которыми обладает тренер',
        type: new GraphQLList(new GraphQLNonNull(Table1x1Type)),
      },
      tables2x2: {
        description: 'Турнирные таблицы 2х2 которыми обладает тренер',
        type: new GraphQLList(new GraphQLNonNull(Table2x2Type)),
      },
      tablesExercise: {
        description: 'Турнирные таблицы упражнений которыми обладает тренер',
        type: new GraphQLList(new GraphQLNonNull(TableExerciseType)),
      }
    };
  },
});
