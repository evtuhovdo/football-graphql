import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLID,
} from 'graphql';

export default new GraphQLObjectType({
  name: 'ProfileOldAvatarType',
  description: 'Старые фотографии профиля',
  fields() {
    return {
      id: {
        type: GraphQLID,
      },
      url: {
        type: new GraphQLNonNull(GraphQLString),
      },
    };
  },
});
