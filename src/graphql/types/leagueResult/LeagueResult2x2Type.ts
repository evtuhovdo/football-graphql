import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';

import { IResolverContext } from '../../createApolloServer';
import LeagueType from '../LeagueType';
import CompositionType from '../CompositionType';


const LeagueResult2x2Type: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'LeagueResult2x2',
  description: 'Рейтинг для лиги 2х2',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      points: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      position: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      gamesCount: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      winsCount: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      drawCount: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      lossCount: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      league: {
        type: new GraphQLNonNull(LeagueType),
      },
      composition: {
        type: new GraphQLNonNull(CompositionType),
      },
    }
  }
});

export default LeagueResult2x2Type;
