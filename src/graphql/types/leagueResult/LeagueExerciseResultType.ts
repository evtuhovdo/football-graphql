import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import LeagueExerciseResult from '../../../orm/entity/Result/LeagueExerciseResult';

import { IResolverContext } from '../../createApolloServer';
import LeagueType from '../LeagueType';
import CompositionType from '../CompositionType';
import SportsmanType from '../SportsmanType';


const LeagueExerciseResultType: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'LeagueExerciseResult',
  description: 'Рейтинг для лиги упражнений',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      points: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      position: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      league: {
        type: new GraphQLNonNull(LeagueType),
      },
      sportsman: {
        type: new GraphQLNonNull(SportsmanType),
      },
    }
  }
});

export default LeagueExerciseResultType;
