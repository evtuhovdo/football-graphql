import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';

import { IResolverContext } from '../../createApolloServer';
import LeagueType from '../LeagueType';
import SportsmanType from '../SportsmanType';


const LeagueResult1x1Type: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'LeagueResult1x1',
  description: 'Рейтинг для лиги 1х1',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      points: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      position: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      gamesCount: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      winsCount: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      drawCount: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      lossCount: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      league: {
        type: new GraphQLNonNull(LeagueType),
      },
      sportsman: {
        type: new GraphQLNonNull(SportsmanType),
      },
    }
  }
});

export default LeagueResult1x1Type;
