import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLFloat,
  GraphQLInt,
  GraphQLString,
  GraphQLID,
} from 'graphql';
import moment from 'moment-timezone';
import SportsmanPersonalParameterNameEnumType, { ISportsmanPersonalParameterNameEnum } from '../enums/SportsmanPersonalParameterNameEnumType';

export enum ISportsmanPersonalParameterHumanNameEnum {
  FAT_PERCENT = 'Процент жира',
  HEIGHT = 'Рост',
  WEIGHT = 'Вес',
  BICEPS = 'Бицепс',
  CHEST = 'Обхват груди',
  WAIST = 'Обхват талии',
  HIP = 'Обхват бедер',
  SHIN = 'Обхват голени',
}

export enum ISportsmanPersonalParameterHumanUnitNameEnum {
  FAT_PERCENT = '%',
  HEIGHT = 'см',
  WEIGHT = 'см',
  BICEPS = 'см',
  CHEST = 'см',
  WAIST = 'см',
  HIP = 'см',
  SHIN = 'см',
}

const getSportsmanPersonalParamHumanName = (type: ISportsmanPersonalParameterNameEnum) => {
  return ISportsmanPersonalParameterHumanNameEnum[type] || '???';
};

const getSportsmanPersonalParamHumanUnitName = (type: ISportsmanPersonalParameterNameEnum) => {
  return ISportsmanPersonalParameterHumanUnitNameEnum[type] || '???';
};

export default new GraphQLObjectType({
  name: 'SportsmanParam',
  description: 'Параметр спорстмена',
  fields() {
    return {
      id: {
        type: GraphQLID,
      },
      name: {
        type: new GraphQLNonNull(SportsmanPersonalParameterNameEnumType),
      },
      value: {
        type: new GraphQLNonNull(GraphQLFloat),
      },
      prevValue: {
        type: GraphQLFloat,
      },
      deltaPercent: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      date: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (sportsmanParam => moment(sportsmanParam.date).format('DD.MM.YYYY')),
      },
      humanName: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (sportsmanParam => getSportsmanPersonalParamHumanName(sportsmanParam.name)),
      },
      humanUnit: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (sportsmanParam => getSportsmanPersonalParamHumanUnitName(sportsmanParam.name)),
      }
    };
  },
});
