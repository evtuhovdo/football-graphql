import {
  GraphQLObjectType,
  GraphQLUnionType
} from 'graphql';
import MutationFieldErrorType from './MutationFieldErrorType';
import MutationGlobalErrorType from './MutationGlobalErrorType';

interface IResolveMutationErrorType {
  (value: any): GraphQLObjectType
}

const resolveMutationErrorType: IResolveMutationErrorType = (value) => {
  return value.field ? MutationFieldErrorType : MutationGlobalErrorType;
};

const MutationErrorType =  new GraphQLUnionType({
  name: 'MutationErrorType',
  types: [MutationFieldErrorType, MutationGlobalErrorType],
  resolveType: resolveMutationErrorType,
});

export default MutationErrorType;
