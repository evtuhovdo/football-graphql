import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';

export interface IMutationGlobalError {
  message: string;
}

const MutationGlobalErrorType =  new GraphQLObjectType({
  name: 'MutationGlobalErrorType',
  fields() {
    return {
      message: {
        type: new GraphQLNonNull(GraphQLString),
      },
    };
  },
});

export default MutationGlobalErrorType;
