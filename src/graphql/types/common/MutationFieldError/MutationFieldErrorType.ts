import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
} from 'graphql';

export interface IMutationFieldError {
  field: string;
  message: string;
}

const MutationFieldErrorType = new GraphQLObjectType({
  name: 'MutationFieldErrorType',
  fields() {
    return {
      field: {
        type: new GraphQLNonNull(GraphQLString),
      },
      message: {
        type: new GraphQLNonNull(GraphQLString),
      },
    };
  },
});

export default MutationFieldErrorType;
