import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLOutputType,
  GraphQLList,
} from 'graphql';
import MutationErrorType from '../common/MutationFieldError/MutationErrorType';

interface IMutationPayloadWithErrorType {
  (name: string, payloadType: GraphQLOutputType): GraphQLObjectType;
}

const MutationPayloadWithErrorType: IMutationPayloadWithErrorType = (name, payloadType) => new GraphQLObjectType({
  name: `${name}Payload`,
  fields() {
    return {
      payload: {
        type: payloadType,
      },
      errors: {
        type: new GraphQLList(new GraphQLNonNull(MutationErrorType)),
      },
    };
  },
});

export default MutationPayloadWithErrorType;
