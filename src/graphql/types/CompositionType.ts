import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLID,
  GraphQLList,
} from 'graphql';
import CompositionTypeEnumType from '../enums/CompositionTypeEnumType';
import SportsmanType from './SportsmanType';
import TeamType from './TeamType';

export default new GraphQLObjectType({
  name: 'Composition',
  description: 'Состав',
  fields() {
    return {
      id: {
        type: GraphQLID,
      },
      type: {
        type: new GraphQLNonNull(CompositionTypeEnumType),
      },
      maxCapacity: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      minCapacity: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      sportsmans: {
        type: new GraphQLList(GraphQLNonNull(SportsmanType)),
      },
      team: {
        type: new GraphQLNonNull(TeamType),
      }
    };
  },
});
