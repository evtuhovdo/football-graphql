import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLID,
  GraphQLList,
} from 'graphql';
import each from 'lodash/each';
import map from 'lodash/map';
import find from 'lodash/find';
import { Brackets } from 'typeorm';
import Match2x2 from '../../orm/entity/Challenge/Match2x2';
import League1x1Result from '../../orm/entity/Result/League1x1Result';
import League2x2Result from '../../orm/entity/Result/League2x2Result';
import LeagueExerciseResult from '../../orm/entity/Result/LeagueExerciseResult';
import SportsmansPersonalParams from '../../orm/entity/SportsmansPersonalParams';
import SportsmanPersonalParameterNameEnumType from '../enums/SportsmanPersonalParameterNameEnumType';
import LeagueExerciseResultType from './leagueResult/LeagueExerciseResultType';
import LeagueResult1x1Type from './leagueResult/LeagueResult1x1Type';
import LeagueResult2x2Type from './leagueResult/LeagueResult2x2Type';
import ProfileType from './ProfileType';
import SportsmanParamType from './SportsmanParamType';
import TeamType from './TeamType';

export default new GraphQLObjectType({
  name: 'Sportsman',
  description: 'Профиль спортсмена',
  fields() {
    return {
      id: {
        type: GraphQLID,
      },
      profile: {
        type: new GraphQLNonNull(ProfileType),
        resolve: (sportsman) => sportsman,
      },
      team: {
        type: TeamType,
      },
      numberInTeam: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      historyOfParam: {
        args: {
          paramName: {
            type: SportsmanPersonalParameterNameEnumType,
          },
        },
        type: new GraphQLList(new GraphQLNonNull(SportsmanParamType)),
        resolve: async (sportsman, { paramName }, { entityManager }) => {
          const rSportsmansPersonalParams = entityManager.getRepository(SportsmansPersonalParams);

          return rSportsmansPersonalParams.find({
            where: {
              name: paramName,
              sportsman,
            },
            order: {
              date: 'DESC',
            },
            take: 5,
          });
        },
      },
      params: {
        type: new GraphQLList(new GraphQLNonNull(SportsmanParamType)),
        resolve: async (sportsman, args, { entityManager }) => {
          const currentRawData = await entityManager.query(`
            SELECT
                DISTINCT ON ("params"."name") *
            FROM
                "sportsmans_personal_params" "params"
            WHERE
                "params"."sportsmanId" = $1
            ORDER BY
                "params"."name" ASC,
                "params"."date" DESC
            `,
            [sportsman.id],
          );

          const ids = map(currentRawData, item => parseInt(item.id, 10));

          const prevData = await entityManager.query(`
            SELECT
                DISTINCT ON ("params"."name") *
            FROM
                "sportsmans_personal_params" "params"
            WHERE
                "params"."sportsmanId" = $1
                AND NOT ARRAY["params"."id"] <@ ($2)
            ORDER BY
                "params"."name" ASC,
                "params"."date" DESC
            `,
            [sportsman.id, ids],
          );

          const currentData = map(currentRawData, item => ({
            ...item,
            deltaPercent: 0,
          }));

          each(prevData, item => {
            const current = find(currentData, { name: item.name });

            current.prevValue = parseFloat(item.value);
            if (current.prevValue !== 0) {
              current.deltaPercent = Math.ceil((parseFloat(current.value) * 100) / parseFloat(item.value)) - 100;
            }
          });

          return currentData;
        },
      },
      myTeamRating1x1: {
        type: new GraphQLList(new GraphQLNonNull(LeagueResult1x1Type)),
        // args: {
        //   leagueId: {
        //     type: new GraphQLNonNull(GraphQLID),
        //   },
        // },
        resolve: async (sportsman, { leagueId }, { entityManager }) => {
          const team = await sportsman.team;

          return await entityManager
            .getRepository(League1x1Result)
            .createQueryBuilder('res')
            .leftJoinAndSelect('res.league', 'league')
            .leftJoinAndSelect('res.sportsman', 'sportsman')
            .andWhere('sportsman.team_id = :teamId', { teamId: team.id })
            // .andWhere('league.id = :leagueId', { leagueId: leagueId })
            .orderBy('res.position', 'DESC')
            .getMany();
        },
      },
      myTeamRating2x2: {
        type: new GraphQLList(new GraphQLNonNull(LeagueResult2x2Type)),
        // args: {
        //   leagueId: {
        //     type: new GraphQLNonNull(GraphQLID),
        //   },
        // },
        resolve: async (sportsman, { leagueId }, { entityManager }) => {
          const team = await sportsman.team;

          return await entityManager
            .getRepository(League2x2Result)
            .createQueryBuilder('res')
            .leftJoinAndSelect('res.league', 'league')
            .leftJoinAndSelect('res.composition', 'composition')
            .andWhere('composition.team_id = :teamId', { teamId: team.id })
            // .andWhere('league.id = :leagueId', { leagueId: leagueId })
            .orderBy('res.position', 'DESC')
            .getMany();
        },
      },
      myTeamExerciseRating: {
        type: new GraphQLList(new GraphQLNonNull(LeagueExerciseResultType)),
        // args: {
        //   leagueId: {
        //     type: new GraphQLNonNull(GraphQLID),
        //   },
        // },
        resolve: async (sportsman, { leagueId }, { entityManager }) => {
          const team = await sportsman.team;

          return await entityManager
            .getRepository(LeagueExerciseResult)
            .createQueryBuilder('res')
            .leftJoinAndSelect('res.league', 'league')
            .leftJoinAndSelect('res.sportsman', 'sportsman')
            .andWhere('sportsman.team_id = :teamId', { teamId: team.id })
            // .andWhere('league.id = :leagueId', { leagueId: leagueId })
            .orderBy('res.position', 'DESC')
            .getMany();
        },
      },
      paramByName: {
        args: {
          paramName: {
            type: SportsmanPersonalParameterNameEnumType,
          },
        },
        type: SportsmanParamType,
        resolve: async (sportsman, { paramName }, { entityManager }) => {
          const params = await entityManager.find(
            SportsmansPersonalParams,
            {
              where: {
                sportsman,
                name: paramName,
              },
              order: {
                date: 'DESC',
              },
              take: 2,
            },
          );

          if (!params || params.length === 0) {
            return null;
          }

          const prevValue = params[1] ? params[1].value : 0;

          const current = {
            ...params[0],
            prevValue,
            deltaPercent: 0,
          };


          if (current.prevValue !== 0) {
            current.deltaPercent = Math.ceil((parseFloat(current.value) * 100) / parseFloat(prevValue)) - 100;
          }

          return current;
        },
      },
    };
  },
});
