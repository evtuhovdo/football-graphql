import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLList,
  GraphQLNonNull, GraphQLInt,
} from 'graphql';
import ExerciseChallenge from '../../../orm/entity/Challenge/ExerciseChallenge';

import { IResolverContext } from '../../createApolloServer';
import ExerciseChallengeType from '../challenge/ExerciseChallengeType';
import Match2x2Type from '../challenge/Match2x2Type';
import CompositionType from '../CompositionType';
import ExerciseType from '../exercise/ExerciseType';
import LeagueType from '../LeagueType';
import SportsmanType from '../SportsmanType';
import TrainerType from '../TrainerType';


const TableExerciseType: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'TableExercise',
  description: 'Турнирная таблица упражнений',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      number: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      trainer: {
        type: new GraphQLNonNull(TrainerType),
      },
      league: {
        type: new GraphQLNonNull(LeagueType),
      },
      sportsmans: {
        type: new GraphQLList(new GraphQLNonNull(SportsmanType)),
      },
      challenges: {
        type: new GraphQLList(new GraphQLNonNull(ExerciseChallengeType)),
      },
      exercises: {
        type: new GraphQLList(new GraphQLNonNull(ExerciseType)),
      },
    }
  }
});


export default TableExerciseType;
