import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
} from 'graphql';

import { IResolverContext } from '../../createApolloServer';
import LeagueType from '../LeagueType';
import Match1x1Type from '../challenge/Match1x1Type';
import SportsmanType from '../SportsmanType';

import TrainerType from '../TrainerType';


const Table1x1Type: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'Table1x1',
  description: 'Турнирная таблица 1х1',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      number: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      trainer: {
        type: new GraphQLNonNull(TrainerType),
      },
      league: {
        type: new GraphQLNonNull(LeagueType),
      },
      sportsmans: {
        type: new GraphQLList(new GraphQLNonNull(SportsmanType)),
      },
      matches: {
        type: new GraphQLList(new GraphQLNonNull(Match1x1Type)),
      },
    }
  }
});


export default Table1x1Type;
