import {
  GraphQLObjectType,
  GraphQLID,
  GraphQLList,
  GraphQLNonNull, GraphQLInt,
} from 'graphql';

import { IResolverContext } from '../../createApolloServer';
import Match2x2Type from '../challenge/Match2x2Type';
import CompositionType from '../CompositionType';
import LeagueType from '../LeagueType';
import TrainerType from '../TrainerType';


const Table2x2Type: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'Table2x2',
  description: 'Турнирная таблица 2х2',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      number: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      trainer: {
        type: new GraphQLNonNull(TrainerType),
      },
      league: {
        type: new GraphQLNonNull(LeagueType),
      },
      compositions: {
        type: new GraphQLList(new GraphQLNonNull(CompositionType)),
      },
      matches: {
        type: new GraphQLList(new GraphQLNonNull(Match2x2Type)),
      },
    }
  }
});


export default Table2x2Type;
