import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import moment from 'moment-timezone';

import { IResolverContext } from '../../createApolloServer';
import CompositionType from '../CompositionType';
import LeagueType from '../LeagueType';
import Table1x1Type from '../table/Table1x1Type';


const Match2x2Type: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'Match2x2',
  description: 'Единичный матч 2х2',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      date: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (match1x1 => {
          if (!match1x1.date) {
            return '';
          }

          return moment(match1x1.date).format('DD.MM.YYYY');
        }),
      },
      leftScore: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      rightScore: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      compositionLeft: {
        type: new GraphQLNonNull(CompositionType),
      },
      compositionRight: {
        type: new GraphQLNonNull(CompositionType),
      },
      league: {
        type: new GraphQLNonNull(LeagueType),
      },
      table: {
        type: new GraphQLNonNull(Table1x1Type),
      },
    }
  },
});

export default Match2x2Type;
