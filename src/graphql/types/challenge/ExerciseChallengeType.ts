import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import moment from 'moment-timezone';

import { IResolverContext } from '../../createApolloServer';
import ExerciseType from '../exercise/ExerciseType';
import LeagueType from '../LeagueType';
import SportsmanType from '../SportsmanType';
import TableExerciseType from '../table/TableExerciseType';


const ExerciseChallengeType: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'ExerciseChallenge',
  description: 'Единичный подход в упражнении',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      date: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (challenge => {
          if (!challenge.date) {
            return '';
          }

          return moment(challenge.date).format('DD.MM.YYYY');
        }),
      },
      resultValue: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      sportsman: {
        type: new GraphQLNonNull(SportsmanType),
      },
      exercise: {
        type: new GraphQLNonNull(ExerciseType),
      },
      league: {
        type: new GraphQLNonNull(LeagueType),
      },
      table: {
        type: new GraphQLNonNull(TableExerciseType),
      },
    }
  },
});

export default ExerciseChallengeType;
