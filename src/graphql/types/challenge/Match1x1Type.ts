import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLInt,
  GraphQLNonNull,
} from 'graphql';
import moment from 'moment-timezone';

import { IResolverContext } from '../../createApolloServer';
import LeagueType from '../LeagueType';
import SportsmanType from '../SportsmanType';
import Table1x1Type from '../table/Table1x1Type';


const Match1x1Type: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'Match1x1',
  description: 'Единичный матч 1х1',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      date: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (match1x1 => {
          if (!match1x1.date) {
            return '';
          }

          return moment(match1x1.date).format('DD.MM.YYYY');
        }),
      },
      leftScore: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      rightScore: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      sportsmanLeft: {
        type: new GraphQLNonNull(SportsmanType),
      },
      sportsmanRight: {
        type: new GraphQLNonNull(SportsmanType),
      },
      league: {
        type: new GraphQLNonNull(LeagueType),
      },
      table: {
        type: new GraphQLNonNull(Table1x1Type),
      },
    }
  },
});

export default Match1x1Type;
