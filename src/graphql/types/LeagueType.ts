import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLBoolean,
  GraphQLList,
  GraphQLID,
} from 'graphql';
import moment from 'moment-timezone';
import League from '../../orm/entity/League';
import LeagueTypeEnumType from '../enums/LeagueTypeEnumType';
import LeagueExerciseResultType from './leagueResult/LeagueExerciseResultType';
import LeagueResult1x1Type from './leagueResult/LeagueResult1x1Type';
import LeagueResult2x2Type from './leagueResult/LeagueResult2x2Type';
import Table1x1Type from './table/Table1x1Type';
import Table2x2Type from './table/Table2x2Type';

export default new GraphQLObjectType({
  name: 'League',
  description: 'Лига',
  fields() {
    return {
      id: {
        type: GraphQLID,
      },
      name: {
        description: 'Название лиги',
        type: new GraphQLNonNull(GraphQLString),
      },
      startDate: {
        description: 'Дата начала лиги',
        type:new GraphQLNonNull(GraphQLString),
        resolve: ((payload: League) => moment(payload.startDate).format('DD.MM.YYYY')),
      },
      endDate: {
        description: 'Дата окончания лиги',
        type:new GraphQLNonNull(GraphQLString),
        resolve: ((payload: League) => moment(payload.endDate).format('DD.MM.YYYY')),
      },
      type: {
        description: 'Тип лиги',
        type: new GraphQLNonNull(LeagueTypeEnumType),
      },
      tables1x1: {
        description: 'Таблицы 1x1 для этой лиги',
        type: new GraphQLList(new GraphQLNonNull(Table1x1Type)),
      },
      tables2x2: {
        description: 'Таблицы 2x2 для этой лиги',
        type: new GraphQLList(new GraphQLNonNull(Table2x2Type)),
      },
      active: {
        description: 'Признак что лига в данный момент времени идет',
        type:new GraphQLNonNull(GraphQLBoolean),
        resolve: ((payload: League) => {
          const start = moment(payload.startDate).toDate().getTime();
          const end = moment(payload.endDate).toDate().getTime();
          const now = new Date().getTime();

          return !!(now > start && now < end);
        }),
      },
      leagueResults1x1: {
        description: 'Рейтинги для лиги 1х1',
        type: new GraphQLList(new GraphQLNonNull(LeagueResult1x1Type)),
      },
      leagueResults2x2: {
        description: 'Рейтинги для лиги 2х2',
        type: new GraphQLList(new GraphQLNonNull(LeagueResult2x2Type)),
      },
      leagueExerciseResults: {
        description: 'Рейтинги для лиги упражнений',
        type: new GraphQLList(new GraphQLNonNull(LeagueExerciseResultType)),
      },
    };
  },
});
