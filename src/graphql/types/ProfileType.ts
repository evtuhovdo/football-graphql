import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLID,
} from 'graphql';
import moment from 'moment-timezone';
import SexEnumType from '../enums/SexEnumType';

export default new GraphQLObjectType({
  name: 'Profile',
  description: 'Профиль пользователя общий',
  fields() {
    return {
      id: {
        type: GraphQLID,
      },
      avatarURL: {
        type: GraphQLString,
      },
      phone: {
        type: new GraphQLNonNull(GraphQLString),
      },
      email: {
        type: GraphQLString,
      },
      firstName: {
        type: GraphQLString,
      },
      lastName: {
        type: GraphQLString,
      },
      thirdName: {
        type: GraphQLString,
      },
      fullName: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (identity) => [identity.firstName, identity.thirdName, identity.lastName]
          .map(value => !!value ? value.trim() : '')
          .filter(value => !!value && !!value.trim())
          .join(' ')
          .trim(),
      },
      nameAbbr: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (identity) => {
          const { firstName = '', lastName = '' } = identity;

          return `${firstName[0].toUpperCase()}${lastName[0].toUpperCase()}`;
        }
      },
      sex: {
        type: SexEnumType,
      },
      dateOfBirth: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (sportsman => {
          if (!sportsman.dateOfBirth) {
            return '';
          }

          return moment(sportsman.dateOfBirth).format('DD.MM.YYYY');
        }),
      },
      age: {
        type: GraphQLInt,
        resolve: (identity) => {
          const { dateOfBirth } = identity;

          if (!dateOfBirth) {
            return null;
          }

          const nowYear = new Date().getUTCFullYear();
          const yearOfBirth = identity.dateOfBirth.getUTCFullYear();

          return nowYear - yearOfBirth;
        },
      },
    };
  },
});
