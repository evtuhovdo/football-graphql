import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLInt,
  GraphQLID,
} from 'graphql';
import { IResolverContext } from '../../createApolloServer';
import ExerciseDemoVideoType from './ExerciseDemoVideoType';

const ExerciseType: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'Exercise',
  description: 'Упражнение',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      name: {
        type: new GraphQLNonNull(GraphQLString),
      },
      unit: {
        type: new GraphQLNonNull(GraphQLString),
      },
      code: {
        type: new GraphQLNonNull(GraphQLString),
      },
      resultSolutionMethod: {
        type: new GraphQLNonNull(GraphQLString),
      },
      numberOfAttempts: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      timeToAttempt: {
        type: new GraphQLNonNull(GraphQLInt),
      },
      description: {
        type: new GraphQLNonNull(GraphQLString),
      },
      video: {
        type: ExerciseDemoVideoType,
      },
    };
  },
});

export default ExerciseType;
