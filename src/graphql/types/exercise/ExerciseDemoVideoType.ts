import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLBoolean,
  GraphQLID,
} from 'graphql';

export default new GraphQLObjectType({
  name: 'ExerciseDemoVideoType',
  description: 'Видео демонстрирующие упражнение',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      source: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (video) => video.youtubeVideoId || video.videoUrl,
      },
      isYoutubeVideo: {
        type: new GraphQLNonNull(GraphQLBoolean),
        resolve: (video) => !!video.youtubeVideoId,
      },
      header: {
        type: new GraphQLNonNull(GraphQLString),
        resolve: (video) => video.name,
      },
      description: {
        type: new GraphQLNonNull(GraphQLString),
      },
      color: {
        type: new GraphQLNonNull(GraphQLString),
      },
    };
  },
});
