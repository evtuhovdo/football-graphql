import {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLList,
  GraphQLNonNull,
} from 'graphql';
import map from 'lodash/map';

import Sportsman from '../../orm/entity/Sportsman';
import Team from '../../orm/entity/Team';
import { IResolverContext } from '../createApolloServer';
import CompositionType from './CompositionType';
import SportsmanType from './SportsmanType';

import TrainerType from './TrainerType';


const TeamType: GraphQLObjectType<any, IResolverContext, any> = new GraphQLObjectType({
  name: 'Team',
  description: 'Команды участников',
  fields: function () {
    return {
      id: {
        type: GraphQLID,
      },
      name: {
        type: new GraphQLNonNull(GraphQLString),
      },
      trainer: {
        type: new GraphQLNonNull(TrainerType),
      },
      sportsmans: {
        type: new GraphQLList(new GraphQLNonNull(SportsmanType)),
      },
      compositions: {
        type: new GraphQLList(new GraphQLNonNull(CompositionType)),
      },
      toComposition: {
        type: new GraphQLList(new GraphQLNonNull(SportsmanType)),
        resolve: async (team, args, { entityManager }) => {
          const sportsmans = await entityManager.query(`
              SELECT "identity".id from "identity" WHERE "identity".id NOT IN(
              SELECT DISTINCT "composition_sportsmans_identity"."identityId" from "composition_sportsmans_identity" 
              LEFT JOIN composition ON composition.id = "composition_sportsmans_identity"."compositionId"
              LEFT JOIN team ON team.id = composition.team_id
              WHERE team.id = $1) and "identity".team_id = $1 and "identity".type = 'Sportsman'
            `, [team.id]);

          if (!sportsmans || !sportsmans.length) {
            return [];
          }

          const ids = map(sportsmans, (item) => parseInt(item.id, 10));

          return await entityManager.createQueryBuilder(Sportsman, "sp")
            .whereInIds(ids)
            .andWhere('sp.team_id = :teamId', { teamId: team.id })
            .getMany();
        },
      },
    }
  }
});


export default TeamType;
