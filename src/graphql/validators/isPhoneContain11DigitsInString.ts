export const PHONE_LENGTH = 11;

interface IIsPhoneValid {
  (phone: string | undefined | null): boolean;
}

const isPhoneContain11DigitsInString: IIsPhoneValid = (phone) => {
  console.log('phone', phone);
  if (!phone) {
    return false
  }

  const regExpDigits = /^\d+$/;

  if (phone.length !== PHONE_LENGTH) {
    return false;
  }

  return regExpDigits.test(phone);
};

export default isPhoneContain11DigitsInString;
