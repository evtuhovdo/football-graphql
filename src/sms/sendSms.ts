import axios from 'axios';
import get from 'lodash/get';
import isPhoneContain11DigitsInString from '../graphql/validators/isPhoneContain11DigitsInString';

const SMS_API_KEY = process.env.SMS_API_KEY || '';
const SMS_API_EMAIL = process.env.SMS_API_EMAIL || '';
const SMS_API_SIGN = process.env.SMS_API_SIGN || '';

const sendSms = async (text: string, phone: string): Promise<boolean> => {
  if (!isPhoneContain11DigitsInString(phone)) {
    return false;
  }

  const url = `https://${SMS_API_EMAIL}:${SMS_API_KEY}@gate.smsaero.ru/v2/sms/send?number=${phone}&text=${encodeURIComponent(text)}&sign=${SMS_API_SIGN}&channel=DIRECT`;

  console.log('sms url', url);

  try {
    const response = await axios.get(url);

    return !!get(response, 'data.success');
  } catch (e) {
    return false
  }
};

export default sendSms;

