import { IMutationFieldError } from './graphql/types/common/MutationFieldError/MutationFieldErrorType';
import { IMutationGlobalError } from './graphql/types/common/MutationFieldError/MutationGlobalErrorType';

export interface IMutationReturnWithErrors<T> {
  payload?: T | null | undefined,
  errors?: IMutationGlobalError | IMutationFieldError[] | null | undefined;
}
