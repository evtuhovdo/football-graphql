export const OPERATOR_ROLE_NAME = 'operator';
export const CLIENT_ROLE_NAME = 'client';
export const SPORTSMAN_ROLE_NAME = 'sportsman';
export const TRAINER = 'trainer';
export const ADMIN_ROLE_NAME = 'admin';
export const GUEST_ROLE_NAME = 'guest';
